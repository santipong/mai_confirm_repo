import 'dart:convert';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:cool_alert/cool_alert.dart';
import 'package:flutter/material.dart';
import 'package:jwt_decode/jwt_decode.dart';
import 'package:lottie/lottie.dart';
import 'package:mai_confirm_asset/main.dart';
import 'package:mai_confirm_asset/models/login_request.dart';
import 'package:mai_confirm_asset/models/login_response.dart';
import 'package:mai_confirm_asset/services/network_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../widget/action_button.dart';
import '../../utils/constants.dart';

class LoginPage extends StatefulWidget {
  static const String loginKey = 'login';
  final String? token;

  const LoginPage({
    Key? key,
    this.token,
  }) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  String? userId, password;

  @override
  void dispose() {
    _usernameController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    getToken().then((data) {
      setState(() {
        if (data.status == "success") {
          openPlanPage(context);
        }
      });
    }).catchError((e) {
      //do nothing
    });
  }

  @override
  Widget build(BuildContext context) {
    return _loginForm(context);
  }

  Widget buildLoading() {
    return Center(
      child: Padding(
          padding: EdgeInsets.all(10.0), child: CircularProgressIndicator()),
    );
  }

  Widget _loginForm(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Padding(
      padding: EdgeInsets.all(size.height > 770
          ? 64
          : size.height > 670
              ? 32
              : 16),
      child: Center(
        child: Card(
          elevation: 4,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(25),
            ),
          ),
          child: AnimatedContainer(
            duration: Duration(milliseconds: 200),
            height: size.height *
                (size.height > 770
                    ? 0.7
                    : size.height > 670
                        ? 0.8
                        : 0.9),
            width: 500,
            color: Colors.white,
            child: Center(
              child: SingleChildScrollView(
                child: Padding(
                  padding: EdgeInsets.all(40),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                        width: 200,
                        height: 200,
                        child: Lottie.network(
                            'https://assets10.lottiefiles.com/packages/lf20_QpolL2.json'),
                      ),
                      Text(
                        "เข้าสู่ระบบ",
                        style: TextStyle(
                          fontSize: 18,
                          color: Colors.grey[700],
                        ),
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Container(
                        width: 30,
                        child: Divider(
                          color: kPrimaryColor,
                          thickness: 2,
                        ),
                      ),
                      SizedBox(
                        height: 32,
                      ),
                      TextField(
                        controller: _usernameController,
                        decoration: InputDecoration(
                          hintText: 'รหัสพนักงาน',
                          labelText: 'รหัสพนักงาน',
                          suffixIcon: Icon(Icons.person),
                        ),
                      ),
                      SizedBox(
                        height: 32,
                      ),
                      TextField(
                        controller: _passwordController,
                        obscureText: true,
                        decoration: InputDecoration(
                          hintText: 'รหัสผ่าน',
                          labelText: 'รหัสผ่าน',
                          suffixIcon: Icon(
                            Icons.lock_outline,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 64,
                      ),
                      GestureDetector(
                        onTap: () => _login(context),
                        child: actionButton("เข้าสู่ระบบ"),
                      ),
                      SizedBox(
                        height: 32,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "",
                            style: TextStyle(
                              color: Colors.grey,
                              fontSize: 14,
                            ),
                          ),
                          SizedBox(
                            width: 8,
                          ),
                          GestureDetector(
                            onTap: () {
                              showErrorDialog("ยังไม่ได้เชื่อมหน้าจ้า");
                            },
                            child: Row(
                              children: [
                                Text(
                                  "",
                                  style: TextStyle(
                                    color: kPrimaryColor,
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                SizedBox(
                                  width: 8,
                                ),
                                // Icon(
                                //   Icons.arrow_forward,
                                //   color: kPrimaryColor,
                                // ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  void showErrorDialog(String? desc) {
    AwesomeDialog(
      context: context,
      dialogType: DialogType.error,
      animType: AnimType.scale,
      width: 500,
      title: 'เกิดข้อผิดพลาด',
      desc: desc,
      autoHide: Duration(seconds: 3),
    )..show();
  }

  void doNotthing() {}

  void _login(BuildContext context) async {
    if (_usernameController.text.isEmpty || _passwordController.text.isEmpty) {
      showErrorDialog("กรุณากรอกรหัสพนักงาน และรหัสผ่านให้ถูกต้อง");
    } else {
      CoolAlert.show(
        context: context,
        type: CoolAlertType.loading,
        text: "Loading...",
        width: 500.0,
      );
      LoginRequest request = LoginRequest(
          userId: _usernameController.text, password: _passwordController.text);
      LoginResponse response = await NetworkService().login(request);
      Navigator.pop(context);
      if (response.status == NetworkService.SUCCESS) {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        await prefs.setString(LoginPage.loginKey, jsonEncode(response));
        openPlanPage(context);
      } else {
        showErrorDialog(response.message);
      }
    }
  }

  void openPlanPage(BuildContext context) {
    Navigator.pushNamedAndRemoveUntil(context, Routes.createPlan, (r) => false);
  }

  Future<LoginResponse> getToken() async {
    String? empId = _getEmployeeId(widget.token!);
    LoginRequest request = LoginRequest(userId: empId, password: empId);
    LoginResponse response = await NetworkService().login(request);
    if (response.status == NetworkService.SUCCESS) {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      await prefs.setString(LoginPage.loginKey, jsonEncode(response));
    }
    return response;
  }

  String? _getEmployeeId(String token) {
    try {
      Map<String, dynamic> payload = Jwt.parseJwt(token);
      return payload["unique_name"];
    } catch (e) {
      return "";
    }
  }
}
