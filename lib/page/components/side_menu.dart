import 'package:flutter/material.dart';
import 'package:mai_confirm_asset/models/login_response.dart';
import 'package:mai_confirm_asset/utils/constants.dart';
import 'package:mai_confirm_asset/utils/responsive.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../main.dart';
import 'menu_item.dart';

class SideMenu extends StatelessWidget {
  final String currentPage;
  final LoginResponse? loginResponse;

  const SideMenu({
    Key? key,
    required this.currentPage,
    this.loginResponse,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      color: Colors.white,
      child: SafeArea(
        child: SingleChildScrollView(
          padding: EdgeInsets.all(20),
          child: Column(
            children: [
              SizedBox(height: 10),
              NavItem(
                title: 'แผนงาน',
                textColor: kTextColor,
                isSelected: currentPage == PAGE_CREATE_PLAN,
                tapEvent: () {
                  openPlanPage(context);
                },
              ),
              SizedBox(height: 10),
              NavItem(
                title: 'รายงาน',
                textColor: kTextColor,
                isSelected: currentPage == PAGE_REPORT,
                tapEvent: () {
                  openReportPage(context);
                },
              ),
              SizedBox(height: 50),
              NavItem(
                title: 'ออกจากระบบ',
                textColor: kRed,
                isSelected: false,
                tapEvent: () {
                  logout(context);
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}

logout(BuildContext context) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.clear();
  Navigator.pushNamedAndRemoveUntil(context, Routes.login, (r) => false);
}

void openPlanPage(BuildContext context) {
  Navigator.pushNamedAndRemoveUntil(context, Routes.createPlan, (r) => false);
}

void openReportPage(BuildContext context) {
  Navigator.pushNamedAndRemoveUntil(context, Routes.report, (r) => false);
}
