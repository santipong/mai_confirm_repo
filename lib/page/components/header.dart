import 'package:flutter/material.dart';
import 'package:mai_confirm_asset/models/login_response.dart';
import 'package:mai_confirm_asset/utils/constants.dart';
import 'package:mai_confirm_asset/utils/responsive.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../main.dart';
import 'menu_item.dart';

class Header extends StatelessWidget {
  final String currentPage;
  final LoginResponse loginResponse;

  const Header({
    Key? key,
    required this.currentPage,
    required this.loginResponse,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 20, horizontal: 40),
      child: Row(
        children: <Widget>[
          SizedBox(width: 10),
          Text(
            "MAI ยืนยันยอดลูกหนี้",
            style: TextStyle(fontSize: 18),
          ),
          Spacer(),
          if (!isMobile(context))
            Row(
              children: [
                NavItem(
                  title: 'แผนงาน',
                  textColor: kTextColor,
                  isSelected: currentPage == PAGE_CREATE_PLAN,
                  tapEvent: () {
                    openPlanPage(context);
                  },
                ),
                NavItem(
                  title: 'รายงาน',
                  textColor: kTextColor,
                  isSelected: currentPage == PAGE_REPORT,
                  tapEvent: () {
                    openReportPage(context);
                  },
                ),
                NavItem(
                  title: loginResponse.user!.empName,
                  textColor: kTextColor,
                  isSelected: false,
                  tapEvent: () {},
                ),
                NavItem(
                  title: 'ออกจากระบบ',
                  textColor: kRed,
                  isSelected: false,
                  tapEvent: () {
                    logout(context);
                  },
                ),
              ],
            ),
          if (isMobile(context))
            IconButton(
                icon: Icon(Icons.menu),
                onPressed: () {
                  Scaffold.of(context).openEndDrawer();
                })
        ],
      ),
    );
  }

  logout(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.clear();
    Navigator.pushNamedAndRemoveUntil(context, Routes.login, (r) => false);
  }

  void openPlanPage(BuildContext context) {
    Navigator.pushNamedAndRemoveUntil(context, Routes.createPlan, (r) => false);
  }

  void openReportPage(BuildContext context) {
    Navigator.pushNamedAndRemoveUntil(context, Routes.report, (r) => false);
  }
}
