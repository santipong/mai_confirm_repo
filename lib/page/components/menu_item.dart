import 'package:flutter/material.dart';
import 'package:mai_confirm_asset/utils/constants.dart';

class NavItem extends StatelessWidget {
  const NavItem({
    Key? key,
    required this.title,
    required this.tapEvent,
    required this.textColor,
    required this.isSelected,
  }) : super(key: key);

  final String? title;
  final GestureTapCallback tapEvent;
  final Color textColor;
  final bool isSelected;

  @override
  Widget build(BuildContext context) {
    return isSelected ? selectedMenu() : unselectedMenu();
  }

  Widget selectedMenu() {
    return InkWell(
      onTap: tapEvent,
      hoverColor: Colors.transparent,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 15),
        child: Container(
          decoration: BoxDecoration(
              color: kPrimaryColor,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(5.0),
                topRight: Radius.circular(5.0),
                bottomLeft: Radius.circular(5.0),
                bottomRight: Radius.circular(5.0),
              )),
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
            child: Text(
              title!,
              style: TextStyle(color: kWhite, fontWeight: FontWeight.w300),
            ),
          ),
        ),
      ),
    );
  }

  Widget unselectedMenu() {
    return InkWell(
      onTap: tapEvent,
      hoverColor: Colors.transparent,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 15),
        child: Text(
          title!,
          style: TextStyle(color: textColor, fontWeight: FontWeight.w300),
        ),
      ),
    );
  }
}
