import 'package:flutter/material.dart';
import 'package:mai_confirm_asset/models/employee_response.dart';
import 'package:mai_confirm_asset/utils/home_page_app_theme.dart';

class KeeperItem extends StatelessWidget {
  final Employees? employees;
  final Function onPress;

  const KeeperItem({
    Key? key,
    required this.onPress,
    this.employees,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String profile =
        "https://ss.thaibev.com/mai-confirm-asset/api/getEmployeeProfile/" +
            employees!.empId.toString();
    return InkWell(
      onTap: onPress as void Function()?,
      child: Container(
        child: Column(
          children: <Widget>[
            Padding(
              padding:
                  const EdgeInsets.only(left: 16, right: 16, top: 0, bottom: 0),
              child: Stack(
                clipBehavior: Clip.none,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 8, bottom: 8),
                    child: Container(
                      decoration: BoxDecoration(
                        color: HomePageAppTheme.white,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(8.0),
                            bottomLeft: Radius.circular(8.0),
                            bottomRight: Radius.circular(8.0),
                            topRight: Radius.circular(8.0)),
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                              color: HomePageAppTheme.grey.withOpacity(0.1),
                              offset: Offset(1.1, 1.1),
                              blurRadius: 10.0),
                        ],
                      ),
                      child: Stack(
                        alignment: Alignment.topLeft,
                        children: <Widget>[
                          ClipRRect(
                            borderRadius:
                                BorderRadius.all(Radius.circular(8.0)),
                            child: SizedBox(
                              height: 74,
                              // child: AspectRatio(
                              //   aspectRatio: 1.714,
                              //   child: Image.asset("assets/images/back.png"),
                              // ),
                            ),
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      left: 90,
                                      right: 16,
                                      top: 25,
                                    ),
                                    child: Text(
                                      employees!.empId.toString() +
                                          " " +
                                          employees!.empName!,
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                        fontFamily: HomePageAppTheme.fontName,
                                        fontWeight: FontWeight.w700,
                                        fontSize: 16,
                                        letterSpacing: 0.0,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  left: 90,
                                  bottom: 12,
                                  top: 4,
                                  right: 16,
                                ),
                                child: Text(
                                  "",
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    fontFamily: HomePageAppTheme.fontName,
                                    fontWeight: FontWeight.w400,
                                    fontSize: 14,
                                    letterSpacing: 0.0,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                    top: 20,
                    left: 16,
                    child: SizedBox(
                      width: 50,
                      height: 50,
                      child: CircleAvatar(
                        radius: 60,
                        backgroundColor: Colors.white,
                        child: CircleAvatar(
                          radius: 55,
                          backgroundImage: NetworkImage(profile),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
