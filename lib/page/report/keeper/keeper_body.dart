import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:mai_confirm_asset/models/employee_response.dart';
import 'package:mai_confirm_asset/models/login_response.dart';
import 'package:mai_confirm_asset/page/report/keeper/keeper_item.dart';
import 'package:mai_confirm_asset/services/api_client.dart';
import 'package:mai_confirm_asset/services/network_service.dart';
import 'dart:html' as html;

class KeeperBody extends StatefulWidget {
  final LoginResponse? loginResponse;
  final String? periodId;

  const KeeperBody({
    Key? key,
    this.loginResponse,
    this.periodId,
  }) : super(key: key);

  @override
  _KeeperBodyState createState() => _KeeperBodyState();
}

class _KeeperBodyState extends State<KeeperBody> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<EmployeeResponse>(
      future: NetworkService().getEmployeeByPeriod(widget.periodId!),
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return buildError(context);
        }
        if (snapshot.hasData) {
          final employeeResponse = snapshot.data!;
          if (employeeResponse.status == ApiClient.STATUS_SUCCESS) {
            return buildSuccess(context, employeeResponse);
          } else {
            return buildError(context);
          }
        }
        return buildLoading();
      },
    );
  }

  Widget buildLoading() {
    return Center(
      child: Padding(
          padding: EdgeInsets.all(10.0), child: CircularProgressIndicator()),
    );
  }

  Widget buildError(context) {
    return Center(
      child: Text("Error"),
    );
  }

  Widget buildSuccess(context, EmployeeResponse response) {
    var widgets = new List<Widget>.empty(growable: true);
    for (var employee in response.employees!) {
      widgets.add(KeeperItem(
        onPress: () {
          openInANewTab(
              ApiClient.DOWNLOAD_KEEPER_REPORT +
                  widget.periodId! +
                  "/" +
                  employee.compCode! +
                  "/" +
                  employee.empId.toString());
        },
        employees: employee,
      ));
    }
    return Container(
      child: Column(
        children: widgets,
      ),
    );
  }

  openInANewTab(url) {
    html.window.open(url, 'dowloadReport');
  }
}
