import 'package:flutter/material.dart';
import 'package:mai_confirm_asset/utils/home_page_app_theme.dart';

class MenuView extends StatelessWidget {
  final String title, iconPath;
  final Function onPress;

  const MenuView({
    Key? key,
    required this.title,
    required this.iconPath,
    required this.onPress,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPress as void Function()?,
      child: Container(
        child: Column(
          children: <Widget>[
            Padding(
              padding:
                  const EdgeInsets.only(left: 16, right: 16, top: 0, bottom: 0),
              child: Stack(
                clipBehavior: Clip.none,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 8, bottom: 8),
                    child: Container(
                      decoration: BoxDecoration(
                        color: HomePageAppTheme.white,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(8.0),
                            bottomLeft: Radius.circular(8.0),
                            bottomRight: Radius.circular(8.0),
                            topRight: Radius.circular(8.0)),
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                              color: HomePageAppTheme.grey.withOpacity(0.1),
                              offset: Offset(1.1, 1.1),
                              blurRadius: 10.0),
                        ],
                      ),
                      child: Stack(
                        alignment: Alignment.topLeft,
                        children: <Widget>[
                          ClipRRect(
                            borderRadius:
                                BorderRadius.all(Radius.circular(8.0)),
                            child: SizedBox(
                              height: 74,
                              // child: AspectRatio(
                              //   aspectRatio: 1.714,
                              //   child: Image.asset("assets/images/back.png"),
                              // ),
                            ),
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      left: 90,
                                      right: 16,
                                      top: 25,
                                    ),
                                    child: Text(
                                      title,
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                        fontFamily: HomePageAppTheme.fontName,
                                        fontWeight: FontWeight.w700,
                                        fontSize: 16,
                                        letterSpacing: 0.0,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  left: 100,
                                  bottom: 12,
                                  top: 4,
                                  right: 16,
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                    top: 20,
                    left: 16,
                    child: SizedBox(
                      width: 50,
                      height: 50,
                      child: Image.asset(iconPath),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
