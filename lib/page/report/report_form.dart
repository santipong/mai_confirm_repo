import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:lottie/lottie.dart';
import 'package:mai_confirm_asset/models/login_response.dart';
import 'package:mai_confirm_asset/models/plan_response.dart';
import 'package:mai_confirm_asset/page/plan/components/plan_item.dart';
import 'package:mai_confirm_asset/page/report/components/menu_view.dart';
import 'package:mai_confirm_asset/page/report/department/dialog_select_plan.dart';
import 'package:mai_confirm_asset/page/report/dialog_select_plan.dart';
import 'package:mai_confirm_asset/page/report/different/dialog_select_plan_comp.dart';
import 'package:mai_confirm_asset/services/api_client.dart';
import 'package:mai_confirm_asset/services/network_service.dart';
import 'package:mai_confirm_asset/utils/constants.dart';
import 'package:mai_confirm_asset/utils/responsive.dart';

class ReportForm extends StatefulWidget {
  final LoginResponse? loginResponse;

  const ReportForm({
    Key? key,
    this.loginResponse,
  }) : super(key: key);

  @override
  _ReportFormState createState() => _ReportFormState();
}

class _ReportFormState extends State<ReportForm> {
  @override
  Widget build(BuildContext context) {
    int rowCount = 0;
    if (isMobile(context)) {
      rowCount = 1;
    } else if (isTab(context)) {
      rowCount = 2;
    } else {
      rowCount = 2;
    }

    return buildSuccess(context, rowCount);
  }

  Widget buildSuccess(context, int rowCount) {
    var _crossAxisSpacing = 10.0;
    var _screenWidth = MediaQuery.of(context).size.width;
    var _crossAxisCount = rowCount;
    var _width = (_screenWidth - ((_crossAxisCount - 1) * _crossAxisSpacing)) /
        _crossAxisCount;
    var cellHeight = 100;
    var _aspectRatio = _width / cellHeight;

    var planCards = List<Widget>.empty(growable: true);
    planCards.add(
      MenuView(
        title: "สรุปผลการตรวจยืนยันยอดลูกหนี้รายพนักงาน",
        iconPath: "assets/image/xls.png",
        onPress: () {
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return DialogSelectPlan(
                  loginResponse: widget.loginResponse,
                );
              }).then((val) {
            setState(() {});
          });
        },
      ),
    );
    planCards.add(
      MenuView(
        title: "สรุปผลการตรวจยืนยันยอดลูกหนี้ POP/POS อุปกรณ์เบียร์สด",
        iconPath: "assets/image/xls.png",
        onPress: () {
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return DialogSelectPlanDepartment(
                    loginResponse: widget.loginResponse, type: 1);
              }).then((val) {
            setState(() {});
          });
        },
      ),
    );
    planCards.add(
      MenuView(
        title:
            "สรุปผลต่างยอดยืม POP POS / อุปกรณ์เบียร์สด ตามบัญชีเทียบตามตรวจนับ",
        iconPath: "assets/image/xls.png",
        onPress: () {
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return DialogSelectPlanCompany(
                  loginResponse: widget.loginResponse,
                );
              }).then((val) {
            setState(() {});
          });
        },
      ),
    );
    planCards.add(
      MenuView(
        title: "สรุปผลการตรวจยืนยันยอดลูกหนี้ (รูปภาพ)",
        iconPath: "assets/image/xls.png",
        onPress: () {
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return DialogSelectPlanDepartment(
                  loginResponse: widget.loginResponse,
                  type: 2,
                );
              }).then((val) {
            setState(() {});
          });
        },
      ),
    );

    return Expanded(
      child: Container(
        decoration: BoxDecoration(color: kGrayBg),
        child: GridView.count(
          primary: false,
          padding: const EdgeInsets.all(20),
          childAspectRatio: _aspectRatio,
          crossAxisSpacing: _crossAxisSpacing,
          mainAxisSpacing: 10,
          crossAxisCount: _crossAxisCount,
          scrollDirection: Axis.vertical,
          children: planCards,
        ),
      ),
    );
  }

  Widget planCard(context, Periods periods) {
    return PlanItem(
      periods: periods,
      onItemClick: () {},
    );
  }
}
