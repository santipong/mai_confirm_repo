import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:mai_confirm_asset/main.dart';
import 'package:mai_confirm_asset/models/login_response.dart';
import 'package:mai_confirm_asset/page/components/header.dart';
import 'package:mai_confirm_asset/page/login/login_page.dart';
import 'package:mai_confirm_asset/page/components/side_menu.dart';
import 'package:mai_confirm_asset/page/report/report_form.dart';
import 'package:mai_confirm_asset/utils/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ReportPage extends StatefulWidget {
  @override
  _ReportPageState createState() => _ReportPageState();
}

class _ReportPageState extends State<ReportPage> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<LoginResponse>(
      future: getLoginData(),
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          Navigator.pushNamed(context, Routes.login);
        }
        if (snapshot.hasData) {
          final LoginResponse? loginResponse = snapshot.data;
          if (loginResponse != null) {
            return body(context, loginResponse);
          } else {
            Navigator.pushNamed(context, Routes.login);
          }
        }
        return buildLoading();
      },
    );
  }

  Widget buildLoading() {
    return Center(
      child: Padding(
          padding: EdgeInsets.all(10.0), child: CircularProgressIndicator()),
    );
  }

  Widget buildError() {
    return Center(
      child: Padding(
          padding: EdgeInsets.all(10.0), child: Text("ไม่พบข้อมูลผู้ใช้งาน")),
    );
  }

  Widget body(BuildContext context, LoginResponse loginResponse) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
        endDrawer: ConstrainedBox(
          constraints: BoxConstraints(maxWidth: 300),
          child: SideMenu(
            currentPage: PAGE_REPORT,
          ),
        ),
        body: SafeArea(
          child: Container(
            decoration: BoxDecoration(color: Colors.white),
            width: size.width,
            constraints: BoxConstraints(minHeight: size.height),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Header(
                  currentPage: PAGE_REPORT,
                  loginResponse: loginResponse,
                ),
                loginResponse != null
                    ? ReportForm(
                        loginResponse: loginResponse,
                      )
                    : buildError(),
                //Footer(),
              ],
            ),
          ),
        ));
  }

  Future<LoginResponse> getLoginData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String rawJson = prefs.getString(LoginPage.loginKey)!;
    Map<String, dynamic> json = jsonDecode(rawJson);
    return LoginResponse.fromJson(json);
  }
}
