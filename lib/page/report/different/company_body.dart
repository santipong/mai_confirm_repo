import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:mai_confirm_asset/models/company_response.dart';
import 'package:mai_confirm_asset/models/login_response.dart';
import 'package:mai_confirm_asset/page/report/different/company_item.dart';
import 'package:mai_confirm_asset/services/api_client.dart';
import 'package:mai_confirm_asset/services/network_service.dart';
import 'dart:html' as html;

class CompanyBody extends StatefulWidget {
  final LoginResponse? loginResponse;
  final String? periodId;

  const CompanyBody({
    Key? key,
    this.loginResponse,
    this.periodId,
  }) : super(key: key);

  @override
  _CompanyBodyState createState() => _CompanyBodyState();
}

class _CompanyBodyState extends State<CompanyBody> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<CompaniesResponse>(
      future: NetworkService().getCompany(widget.loginResponse!.user!.region!),
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return buildError(context);
        }
        if (snapshot.hasData) {
          final companiesResponse = snapshot.data!;
          if (companiesResponse.status == ApiClient.STATUS_SUCCESS) {
            return buildSuccess(context, companiesResponse);
          } else {
            return buildError(context);
          }
        }
        return buildLoading();
      },
    );
  }

  Widget buildLoading() {
    return Center(
      child: Padding(
          padding: EdgeInsets.all(10.0), child: CircularProgressIndicator()),
    );
  }

  Widget buildError(context) {
    return Center(
      child: Text("Error"),
    );
  }

  Widget buildSuccess(context, CompaniesResponse response) {
    var widgets = new List<Widget>.empty(growable: true);
    for (var company in response.companies!) {
      widgets.add(CompanyItem(
        onPress: ()  {
          openInANewTab(
              ApiClient.DOWNLOAD_DIFF_REPORT +
                  widget.periodId! +
                  "/" +
                  company.region.toString() +
                  "/" +
                  company.compCode.toString());
        },
        company: company,
      ));
    }
    return Container(
      child: Column(
        children: widgets,
      ),
    );
  }

  openInANewTab(url) {
    html.window.open(url, 'dowloadReport');
  }
}
