import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:mai_confirm_asset/models/login_response.dart';
import 'package:mai_confirm_asset/models/plan_response.dart';
import 'package:mai_confirm_asset/page/report/department/dialog_select_department.dart';
import 'package:mai_confirm_asset/page/report/keeper/dialog_select_keeper.dart';
import 'package:mai_confirm_asset/page/report/keeper/plan_body_item.dart';
import 'package:mai_confirm_asset/services/api_client.dart';
import 'package:mai_confirm_asset/services/network_service.dart';

class DepartmentPlanBody extends StatefulWidget {
  final LoginResponse? loginResponse;
  final int? type;

  const DepartmentPlanBody({Key? key, this.loginResponse, this.type})
      : super(key: key);

  @override
  _DepartmentPlanBodyState createState() => _DepartmentPlanBodyState();
}

class _DepartmentPlanBodyState extends State<DepartmentPlanBody> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<PlanResponse>(
      future: NetworkService().getPlan(widget.loginResponse!.avaliableRegion!),
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return buildError(context);
        }
        if (snapshot.hasData) {
          final planResponse = snapshot.data!;
          if (planResponse.status == ApiClient.STATUS_SUCCESS) {
            return buildSuccess(context, planResponse);
          } else {
            return buildError(context);
          }
        }
        return buildLoading();
      },
    );
  }

  Widget buildLoading() {
    return Center(
      child: Padding(
          padding: EdgeInsets.all(10.0), child: CircularProgressIndicator()),
    );
  }

  Widget buildError(context) {
    return Center(
      child: Text("Error"),
    );
  }

  Widget buildSuccess(context, PlanResponse response) {
    var widgets = new List<Widget>.empty(growable: true);
    for (var period in response.periods!) {
      widgets.add(PlanBodyItem(
        onPress: () {
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return DialogSelectDepartment(
                    loginResponse: widget.loginResponse,
                    periodId: period.periodId.toString(),
                    type: widget.type);
              }).then((val) {
            //setState(() {});
          });
        },
        periods: period,
      ));
    }
    return Container(
      child: Column(
        children: widgets,
      ),
    );
  }
}
