import 'package:flutter/material.dart';
import 'package:mai_confirm_asset/models/department_response.dart';
import 'package:mai_confirm_asset/utils/home_page_app_theme.dart';

class DepartmentItem extends StatelessWidget {
  final Departments? departments;
  final Function onPress;

  const DepartmentItem({
    Key? key,
    required this.onPress,
    this.departments,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPress as void Function()?,
      child: Container(
        child: Column(
          children: <Widget>[
            Padding(
              padding:
                  const EdgeInsets.only(left: 16, right: 16, top: 0, bottom: 0),
              child: Stack(
                clipBehavior: Clip.none,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 8, bottom: 8),
                    child: Container(
                      decoration: BoxDecoration(
                        color: HomePageAppTheme.white,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(8.0),
                            bottomLeft: Radius.circular(8.0),
                            bottomRight: Radius.circular(8.0),
                            topRight: Radius.circular(8.0)),
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                              color: HomePageAppTheme.grey.withOpacity(0.1),
                              offset: Offset(1.1, 1.1),
                              blurRadius: 10.0),
                        ],
                      ),
                      child: Stack(
                        alignment: Alignment.topLeft,
                        children: <Widget>[
                          ClipRRect(
                            borderRadius:
                                BorderRadius.all(Radius.circular(8.0)),
                            child: SizedBox(
                              height: 74,
                              // child: AspectRatio(
                              //   aspectRatio: 1.714,
                              //   child: Image.asset("assets/images/back.png"),
                              // ),
                            ),
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      left: 90,
                                      right: 16,
                                      top: 25,
                                    ),
                                    child: Text(
                                      "บริษัท " +
                                          _getCompanyName(
                                              departments!.compCode) +
                                          " จำกัด - ศูนย์" +
                                          departments!.center!,
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                        fontFamily: HomePageAppTheme.fontName,
                                        fontWeight: FontWeight.w700,
                                        fontSize: 16,
                                        letterSpacing: 0.0,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  left: 90,
                                  bottom: 12,
                                  top: 4,
                                  right: 16,
                                ),
                                child: Text(
                                  "",
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    fontFamily: HomePageAppTheme.fontName,
                                    fontWeight: FontWeight.w400,
                                    fontSize: 14,
                                    letterSpacing: 0.0,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                    top: 20,
                    left: 16,
                    child: SizedBox(
                        width: 50,
                        height: 50,
                        child: Image.asset("assets/image/modern-house.png")),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  String _getCompanyName(String? compCode) {
    switch (compCode) {
      case "5800":
        return "ป้อมกิจการ";
      case "6400":
        return "นำกิจการ";
      case "7800":
        return "ป้อมทิพย์";
      case "6200":
        return "นำยุค";
      case "5700":
        return "ป้อมบูรพา";
      case "6300":
        return "นำธุรกิจ";
      case "6800":
        return "ป้อมพลัง";
      case "8700":
        return "นำรุ่งโรจน์";
      case "5900":
        return "ป้อมคลัง";
      case "6500":
        return "นำพลัง";
      case "6000":
        return "ป้อมโชค";
      case "6600":
        return "นำเมือง";
      case "6900":
        return "ป้อมนคร";
      case "7900":
        return "นำทิพย์";
      case "6100":
        return "ป้อมเจริญ";
      case "6700":
        return "นำนคร";
      case "3200":
        return "มีชัยมีโชค";
    }
    return "แคชแวน";
  }
}
