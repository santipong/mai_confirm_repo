import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mai_confirm_asset/models/login_response.dart';
import 'package:mai_confirm_asset/page/report/department/report_type_body.dart';
import 'package:mai_confirm_asset/utils/constants.dart';

class DialogSelectReportType extends StatefulWidget {
  final LoginResponse? loginResponse;
  final String? periodId;
  final String? compCode;
  final String? center;
  final int? type;

  const DialogSelectReportType(
      {Key? key,
      this.loginResponse,
      this.periodId,
      this.compCode,
      this.center,
      this.type})
      : super(key: key);

  @override
  _DialogSelectReportTypeState createState() => _DialogSelectReportTypeState();
}

class _DialogSelectReportTypeState extends State<DialogSelectReportType> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(kPadding),
      ),
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: contentBox(context),
    );
  }

  contentBox(context) {
    return Stack(
      children: <Widget>[
        Container(
          width: 800,
          padding: EdgeInsets.only(
            left: kPadding,
            top: 20,
            right: kPadding,
            bottom: kAvatarRadius,
          ),
          margin: EdgeInsets.only(top: kAvatarRadius),
          decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              color: Colors.white,
              borderRadius: BorderRadius.circular(kPadding),
              boxShadow: [
                BoxShadow(
                    color: Colors.black26,
                    offset: Offset(0, 10),
                    blurRadius: 10),
              ]),
          child: SafeArea(
            child: Scrollbar(
              child: SingleChildScrollView(
                child: Form(
                  key: _formKey,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Stack(
                        children: <Widget>[
                          InkWell(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: Icon(
                              Icons.arrow_back_sharp,
                            ),
                          ),
                          Center(
                            child: Text(
                              "บริษัท " +
                                  _getCompanyName(widget.compCode) +
                                  " จำกัด - ศูนย์" +
                                  widget.center!,
                              style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                          )
                        ],
                      ),
                      SizedBox(height: 22),
                      _form(),
                      SizedBox(height: 22),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _form() {
    return Column(
      children: [
        SizedBox(height: 30),
        ReportTypeBody(
          loginResponse: widget.loginResponse,
          periodId: widget.periodId,
          compCode: widget.compCode,
          center: widget.center,
          type: widget.type,
        )
      ],
    );
  }

  String _getCompanyName(String? compCode) {
    switch (compCode) {
      case "5800":
        return "ป้อมกิจการ";
      case "6400":
        return "นำกิจการ";
      case "7800":
        return "ป้อมทิพย์";
      case "6200":
        return "นำยุค";
      case "5700":
        return "ป้อมบูรพา";
      case "6300":
        return "นำธุรกิจ";
      case "6800":
        return "ป้อมพลัง";
      case "8700":
        return "นำรุ่งโรจน์";
      case "5900":
        return "ป้อมคลัง";
      case "6500":
        return "นำพลัง";
      case "6000":
        return "ป้อมโชค";
      case "6600":
        return "นำเมือง";
      case "6900":
        return "ป้อมนคร";
      case "7900":
        return "นำทิพย์";
      case "6100":
        return "ป้อมเจริญ";
      case "6700":
        return "นำนคร";
    }
    return "แคชแวน";
  }
}
