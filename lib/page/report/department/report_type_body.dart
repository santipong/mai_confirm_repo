import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:mai_confirm_asset/models/department_response.dart';
import 'package:mai_confirm_asset/models/login_response.dart';
import 'package:mai_confirm_asset/page/report/components/menu_view.dart';
import 'package:mai_confirm_asset/page/report/department/department_item.dart';
import 'package:mai_confirm_asset/services/api_client.dart';
import 'package:mai_confirm_asset/services/network_service.dart';
import 'dart:html' as html;

class ReportTypeBody extends StatefulWidget {
  final LoginResponse? loginResponse;
  final String? periodId;
  final String? compCode;
  final String? center;
  final int? type;

  const ReportTypeBody({
    Key? key,
    this.loginResponse,
    this.periodId,
    this.compCode,
    this.center,
    this.type,
  }) : super(key: key);

  @override
  _ReportTypeBodyState createState() => _ReportTypeBodyState();
}

class _ReportTypeBodyState extends State<ReportTypeBody> {
  @override
  Widget build(BuildContext context) {
    return buildSuccess(context);
  }

  Widget buildLoading() {
    return Center(
      child: Padding(
          padding: EdgeInsets.all(10.0), child: CircularProgressIndicator()),
    );
  }

  Widget buildError(context) {
    return Center(
      child: Text("Error"),
    );
  }

  Widget buildSuccess(context) {
    var widgets = new List<Widget>.empty(growable: true);
    widgets.add(
      MenuView(
        title: "รายงานทั้งหมด",
        iconPath: "assets/image/xls.png",
        onPress: () {
          openInANewTab(ApiClient.DOWNLOAD_REPORT__BY_COMP_CODE_AND_CENTER +
              widget.periodId! +
              "/" +
              widget.compCode! +
              "/" +
              widget.center!);
        },
      ),
    );
    widgets.add(
      MenuView(
        title: "รายงานแยกตามหน่วย",
        iconPath: "assets/image/xls.png",
        onPress: () {
          openInANewTab(ApiClient.DOWNLOAD_REPORT__BY_DEPARTMENT +
              widget.periodId! +
              "/" +
              widget.compCode! +
              "/" +
              widget.center!);
        },
      ),
    );
    widgets.add(
      MenuView(
        title: "รายงานแยกตามพนักงาน",
        iconPath: "assets/image/xls.png",
        onPress: () {
          openInANewTab(ApiClient.DOWNLOAD_REPORT__BY_EMPLOYEE +
              widget.periodId! +
              "/" +
              widget.compCode! +
              "/" +
              widget.center!);
        },
      ),
    );
    return Container(
      child: Column(
        children: widgets,
      ),
    );
  }

  openInANewTab(url) {
    html.window.open(url, 'dowloadReport');
  }
}
