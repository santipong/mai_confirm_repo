import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:mai_confirm_asset/models/department_response.dart';
import 'package:mai_confirm_asset/models/login_response.dart';
import 'package:mai_confirm_asset/page/report/department/department_item.dart';
import 'package:mai_confirm_asset/page/report/department/dialog_select_report_type.dart';
import 'package:mai_confirm_asset/services/api_client.dart';
import 'package:mai_confirm_asset/services/network_service.dart';
import 'dart:html' as html;

class DepartmentBody extends StatefulWidget {
  final LoginResponse? loginResponse;
  final String? periodId;
  final int? type;

  const DepartmentBody({
    Key? key,
    this.loginResponse,
    this.periodId,
    this.type,
  }) : super(key: key);

  @override
  _DepartmentBodyState createState() => _DepartmentBodyState();
}

class _DepartmentBodyState extends State<DepartmentBody> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<DepartmentResponse>(
      future: NetworkService().getDepartmentByPeriodCenter(widget.periodId!),
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return buildError(context);
        }
        if (snapshot.hasData) {
          final departmentResponse = snapshot.data!;
          if (departmentResponse.status == ApiClient.STATUS_SUCCESS) {
            return buildSuccess(context, departmentResponse);
          } else {
            return buildError(context);
          }
        }
        return buildLoading();
      },
    );
  }

  Widget buildLoading() {
    return Center(
      child: Padding(
          padding: EdgeInsets.all(10.0), child: CircularProgressIndicator()),
    );
  }

  Widget buildError(context) {
    return Center(
      child: Text("Error"),
    );
  }

  Widget buildSuccess(context, DepartmentResponse response) {
    var widgets = new List<Widget>.empty(growable: true);
    for (var departments in response.departments!) {
      widgets.add(DepartmentItem(
        onPress: () {
          if (widget.type != 2) {
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return DialogSelectReportType(
                    loginResponse: widget.loginResponse,
                    periodId: widget.periodId,
                    compCode: departments.compCode,
                    center: departments.center,
                    type: widget.type,
                  );
                }).then((val) {
              //setState(() {});
            });
          } else {
            openInANewTab(
                ApiClient.DOWNLOAD_REPORT_WITH_IMAGE__BY_COMP_CODE_AND_CENTER +
                    widget.periodId! +
                    "/" +
                    departments.compCode! +
                    "/" +
                    departments.center!);
          }
        },
        departments: departments,
      ));
    }
    return Container(
      child: Column(
        children: widgets,
      ),
    );
  }

  openInANewTab(url) {
    html.window.open(url, 'dowloadReport');
  }
}
