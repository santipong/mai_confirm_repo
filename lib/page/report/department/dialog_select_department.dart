import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mai_confirm_asset/models/login_response.dart';
import 'package:mai_confirm_asset/page/report/department/department_body.dart';
import 'package:mai_confirm_asset/utils/constants.dart';

class DialogSelectDepartment extends StatefulWidget {
  final LoginResponse? loginResponse;
  final String? periodId;
  final int? type;

  const DialogSelectDepartment(
      {Key? key, this.loginResponse, this.periodId, this.type})
      : super(key: key);

  @override
  _DialogSelectDepartmentState createState() => _DialogSelectDepartmentState();
}

class _DialogSelectDepartmentState extends State<DialogSelectDepartment> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(kPadding),
      ),
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: contentBox(context),
    );
  }

  contentBox(context) {
    return Stack(
      children: <Widget>[
        Container(
          width: 800,
          padding: EdgeInsets.only(
            left: kPadding,
            top: 20,
            right: kPadding,
            bottom: kAvatarRadius,
          ),
          margin: EdgeInsets.only(top: kAvatarRadius),
          decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              color: Colors.white,
              borderRadius: BorderRadius.circular(kPadding),
              boxShadow: [
                BoxShadow(
                    color: Colors.black26,
                    offset: Offset(0, 10),
                    blurRadius: 10),
              ]),
          child: SafeArea(
            child: Scrollbar(
              child: SingleChildScrollView(
                child: Form(
                  key: _formKey,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Stack(
                        children: <Widget>[
                          InkWell(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: Icon(
                              Icons.arrow_back_sharp,
                            ),
                          ),
                          Center(
                            child: Text(
                              "เลือก บริษัท - ศูนย์ ที่ต้องการ",
                              style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                          )
                        ],
                      ),
                      SizedBox(height: 22),
                      _form(),
                      SizedBox(height: 22),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _form() {
    return Column(
      children: [
        SizedBox(height: 30),
        DepartmentBody(
            loginResponse: widget.loginResponse,
            periodId: widget.periodId,
            type: widget.type)
      ],
    );
  }
}
