import 'dart:ui';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mai_confirm_asset/models/login_response.dart';
import 'package:mai_confirm_asset/models/plan_response.dart';
import 'package:mai_confirm_asset/page/plan/components/edit_plan_dialog.dart';
import 'package:mai_confirm_asset/page/plan/components/task_list.dart';
import 'package:mai_confirm_asset/utils/constants.dart';

class PlanDetailDialog extends StatefulWidget {
  final String? title, descriptions, text, periodId;
  final Image? img;
  final Periods? periods;
  final LoginResponse? loginResponse;

  const PlanDetailDialog({
    Key? key,
    this.title,
    this.descriptions,
    this.text,
    this.img,
    this.loginResponse,
    this.periodId,
    this.periods,
  }) : super(key: key);

  @override
  _PlanDetailDialogState createState() => _PlanDetailDialogState();
}

class _PlanDetailDialogState extends State<PlanDetailDialog> {
  final _formKey = GlobalKey<FormState>();
  String? startDate, endDate, name, fileName, filePath;
  bool? isStartDateSelected, isEndDateSelected;
  PlatformFile? file;
  bool isFirstImage = false;
  bool initData = false;
  bool editable = true;

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(kPadding),
      ),
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: contentBox(context),
    );
  }

  contentBox(context) {
    return Stack(
      children: <Widget>[
        Container(
          width: 800,
          padding: EdgeInsets.only(
              left: kPadding,
              top: kAvatarRadius + kPadding,
              right: kPadding,
              bottom: kAvatarRadius),
          margin: EdgeInsets.only(top: kAvatarRadius),
          decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              color: Colors.white,
              borderRadius: BorderRadius.circular(kPadding),
              boxShadow: [
                BoxShadow(
                    color: Colors.black26,
                    offset: Offset(0, 10),
                    blurRadius: 10),
              ]),
          child: SafeArea(
            child: Scrollbar(
              child: SingleChildScrollView(
                child: Form(
                  key: _formKey,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Text(
                        widget.title!,
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(height: 15),
                      Text(
                        widget.descriptions!,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.w600),
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(height: 22),
                      _bottomView(),
                      SizedBox(height: 22),
                      _form(),
                      SizedBox(height: 22),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _bottomView() {
    return Align(
      alignment: Alignment.center,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          InkWell(
            onTap: () {
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return EditPlanDialog(
                      title: "แก้ไขแผนงาน",
                      descriptions:
                          "กรอกข้อมูลแผนงานที่คุณต้องการแก้ไข และอัปโหลดไฟล์ตาม Template",
                      text: "บันทึก",
                      periods: widget.periods,
                      loginResponse: widget.loginResponse,
                    );
                  }).then((val) {
                setState(() {});
              });
            },
            hoverColor: Colors.transparent,
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 5),
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.orangeAccent[400],
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(5.0),
                      topRight: Radius.circular(5.0),
                      bottomLeft: Radius.circular(5.0),
                      bottomRight: Radius.circular(5.0),
                    )),
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                  child: Text(
                    "แก้ไขแผนงานทั้งหมด",
                    style: TextStyle(
                      color: kWhite,
                      fontWeight: FontWeight.w600,
                      fontSize: 16,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _form() {
    return Column(
      children: [
        TaskListPage(
          periodId: widget.periodId,
        ),
        SizedBox(height: 30),
      ],
    );
  }

  List<String> splitStringByLength(String str, int length) =>
      [str.substring(0, length), str.substring(length)];

  String splitString(String name) {
    if (name.length <= 40) {
      return name;
    }
    return splitStringByLength(name, 37)[0] + "...";
  }
}
