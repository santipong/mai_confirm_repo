import 'dart:io';
import 'dart:typed_data';
import 'dart:ui';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:cool_alert/cool_alert.dart';
import 'package:excel/excel.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mai_confirm_asset/models/login_response.dart';
import 'package:mai_confirm_asset/models/request_create_template.dart';
import 'package:mai_confirm_asset/models/response.dart';
import 'package:mai_confirm_asset/services/network_service.dart';
import 'package:mai_confirm_asset/utils/constants.dart';
import 'package:mai_confirm_asset/utils/date_utils.dart';
import 'package:mai_confirm_asset/utils/home_page_app_theme.dart';
import '../../../main.dart';

class CustomDialogBox extends StatefulWidget {
  final String? title, descriptions, text;
  final Image? img;
  final LoginResponse? loginResponse;

  const CustomDialogBox(
      {Key? key,
      this.title,
      this.descriptions,
      this.text,
      this.img,
      this.loginResponse})
      : super(key: key);

  @override
  _CustomDialogBoxState createState() => _CustomDialogBoxState();
}

class _CustomDialogBoxState extends State<CustomDialogBox> {
  final _formKey = GlobalKey<FormState>();
  String? startDate, endDate, name, fileName, filePath;
  bool? isStartDateSelected, isEndDateSelected;
  Uint8List? file;
  bool isFirstImage = false;
  bool initData = false;
  bool editable = true;
  String err = "";

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(kPadding),
      ),
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: contentBox(context),
    );
  }

  contentBox(context) {
    return Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(
              left: kPadding,
              top: kAvatarRadius + kPadding,
              right: kPadding,
              bottom: kAvatarRadius),
          margin: EdgeInsets.only(top: kAvatarRadius),
          decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              color: Colors.white,
              borderRadius: BorderRadius.circular(kPadding),
              boxShadow: [
                BoxShadow(
                    color: Colors.black26,
                    offset: Offset(0, 10),
                    blurRadius: 10),
              ]),
          child: SafeArea(
            child: Scrollbar(
              child: SingleChildScrollView(
                child: Form(
                  key: _formKey,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Text(
                        widget.title!,
                        style: TextStyle(
                            fontSize: 22, fontWeight: FontWeight.w600),
                      ),
                      SizedBox(height: 15),
                      Text(
                        widget.descriptions!,
                        style: TextStyle(fontSize: 14),
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(height: 22),
                      _form(),
                      SizedBox(height: 22),
                      Align(
                          alignment: Alignment.bottomRight,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              InkWell(
                                onTap: () {
                                  Navigator.pop(context);
                                },
                                hoverColor: Colors.transparent,
                                child: Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 5),
                                  child: Container(
                                    decoration: BoxDecoration(
                                        color: Colors.redAccent,
                                        borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(5.0),
                                          topRight: Radius.circular(5.0),
                                          bottomLeft: Radius.circular(5.0),
                                          bottomRight: Radius.circular(5.0),
                                        )),
                                    child: Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 15, vertical: 10),
                                      child: Text(
                                        "ยกเลิก",
                                        style: TextStyle(
                                          color: kWhite,
                                          fontWeight: FontWeight.w600,
                                          fontSize: 16,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              InkWell(
                                onTap: () {
                                  if (_formKey.currentState!.validate()) {
                                    if (startDate == null || endDate == null) {
                                      _showAlertError(
                                          "กรุณาเลือกวันที่เริ่มต้น และวันที่สิ้นสุดให้ครบถ้วน");
                                    } else if (name == null) {
                                      _showAlertError("กรุณากรอกชื่อแผนงาน");
                                    } else if (convertStringToDateTime(
                                            startDate!)
                                        .isAfter(convertStringToDateTime(
                                            endDate!))) {
                                      _showAlertError(
                                          "วันที่เริ่มต้นไม่สามารถอยู่หลังวันที่สิ้นสุดได้ กรุณากรอกให้ถูกต้อง");
                                    } else if (file == null) {
                                      _showAlertError("กรุณาแนบไฟล์");
                                    } else {
                                      _formKey.currentState!.save();
                                      _save();
                                    }
                                  }
                                },
                                hoverColor: Colors.transparent,
                                child: Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 5),
                                  child: Container(
                                    decoration: BoxDecoration(
                                        color: Colors.green,
                                        borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(5.0),
                                          topRight: Radius.circular(5.0),
                                          bottomLeft: Radius.circular(5.0),
                                          bottomRight: Radius.circular(5.0),
                                        )),
                                    child: Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 15, vertical: 10),
                                      child: Text(
                                        "บันทึก",
                                        style: TextStyle(
                                          color: kWhite,
                                          fontWeight: FontWeight.w600,
                                          fontSize: 16,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          )),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _form() {
    return Column(
      children: [
        _buildStartDatePicker(context),
        _buildEndDatePicker(context),
        SizedBox(height: 30),
        _buildName(),
        SizedBox(height: 30),
        _buildFilePicker(),
        SizedBox(height: 30),
      ],
    );
  }

  Widget _buildStartDatePicker(context) {
    return InkWell(
      onTap: () {
        if (editable) {
          _selectStartDate(context);
        }
      },
      child: SizedBox(
        width: 420,
        height: 160,
        child: Padding(
          padding:
              const EdgeInsets.only(left: 0, right: 0, top: 16, bottom: 18),
          child: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(colors: [
                HexColor("#6F56E8"),
                HomePageAppTheme.nearlyDarkBlue,
              ], begin: Alignment.topLeft, end: Alignment.bottomRight),
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(8.0),
                  bottomLeft: Radius.circular(8.0),
                  bottomRight: Radius.circular(8.0),
                  topRight: Radius.circular(68.0)),
            ),
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "วันที่เริ่มต้น",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontFamily: HomePageAppTheme.fontName,
                      fontWeight: FontWeight.normal,
                      fontSize: 12,
                      letterSpacing: 0.0,
                      color: HomePageAppTheme.white,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0, left: 10),
                    child: Text(
                      startDate == null ? "เลือกวันที่เริ่มต้น" : startDate!,
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontFamily: HomePageAppTheme.fontName,
                        fontWeight: FontWeight.normal,
                        fontSize: 16,
                        letterSpacing: 0.0,
                        color: HomePageAppTheme.white,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 0,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 10, bottom: 10),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          child: SizedBox(),
                        ),
                        Container(
                          decoration: BoxDecoration(
                            color: HomePageAppTheme.nearlyWhite,
                            shape: BoxShape.circle,
                            boxShadow: <BoxShadow>[
                              BoxShadow(
                                  color: HomePageAppTheme.nearlyBlack
                                      .withOpacity(0.4),
                                  offset: Offset(8.0, 8.0),
                                  blurRadius: 8.0),
                            ],
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Icon(
                              Icons.date_range,
                              color: HexColor("#6F56E8"),
                              size: 30,
                            ),
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildEndDatePicker(context) {
    return InkWell(
      onTap: () {
        if (editable) {
          _selectEndDate(context);
        }
      },
      child: SizedBox(
        width: 420,
        height: 160,
        child: Padding(
          padding:
              const EdgeInsets.only(left: 0, right: 0, top: 16, bottom: 18),
          child: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(colors: [
                HexColor("#6F56E8"),
                Colors.red,
              ], begin: Alignment.topLeft, end: Alignment.bottomRight),
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(8.0),
                  bottomLeft: Radius.circular(8.0),
                  bottomRight: Radius.circular(8.0),
                  topRight: Radius.circular(68.0)),
            ),
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "วันที่สิ้นสุด",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontFamily: HomePageAppTheme.fontName,
                      fontWeight: FontWeight.normal,
                      fontSize: 12,
                      letterSpacing: 0.0,
                      color: HomePageAppTheme.white,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0, left: 10),
                    child: Text(
                      endDate == null ? "เลือกวันที่สิ้นสุด" : endDate!,
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontFamily: HomePageAppTheme.fontName,
                        fontWeight: FontWeight.normal,
                        fontSize: 16,
                        letterSpacing: 0.0,
                        color: HomePageAppTheme.white,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 0,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 10, bottom: 10),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          child: SizedBox(),
                        ),
                        Container(
                          decoration: BoxDecoration(
                            color: HomePageAppTheme.nearlyWhite,
                            shape: BoxShape.circle,
                            boxShadow: <BoxShadow>[
                              BoxShadow(
                                  color: HomePageAppTheme.nearlyBlack
                                      .withOpacity(0.4),
                                  offset: Offset(8.0, 8.0),
                                  blurRadius: 8.0),
                            ],
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Icon(
                              Icons.date_range,
                              color: HexColor("#6F56E8"),
                              size: 30,
                            ),
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildName() {
    return SizedBox(
      width: 420,
      child: TextFormField(
        initialValue: name ?? "",
        enabled: editable,
        keyboardType: TextInputType.text,
        onSaved: (newValue) => name = newValue,
        onChanged: (value) {
          name = value;
        },
        decoration: InputDecoration(
          labelText: "ชื่อแผนงาน",
          labelStyle: TextStyle(
            fontSize: 20,
          ),
          hintText: "กรุณากรอกชื่อแผนงาน",
          floatingLabelBehavior: FloatingLabelBehavior.always,
          fillColor: Colors.white,
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
            borderSide: BorderSide(
              color: Colors.blue,
            ),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
            borderSide: BorderSide(
              color: Colors.grey,
              width: 1.0,
            ),
          ),
        ),
      ),
    );
  }

  List<String> splitStringByLength(String str, int length) =>
      [str.substring(0, length), str.substring(length)];

  String splitString(String name) {
    if (name.length <= 40) {
      return name;
    }
    return splitStringByLength(name, 37)[0] + "...";
  }

  Widget _buildFilePicker() {
    return SizedBox(
        width: 420,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Text(
              splitString(fileName ?? "ไฟล์แนบ"),
              style: TextStyle(fontSize: 14, color: Colors.black87),
            ),
            InkWell(
              onTap: () {
                _pickFile();
              },
              hoverColor: Colors.transparent,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 15),
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.blueGrey,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey,
                          blurRadius: 6,
                          offset: Offset(0, 5),
                        ),
                      ],
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(5.0),
                        topRight: Radius.circular(5.0),
                        bottomLeft: Radius.circular(5.0),
                        bottomRight: Radius.circular(5.0),
                      )),
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 25, vertical: 7),
                    child: Text(
                      "เลือกไฟล์แนบ",
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w300,
                        fontSize: 14,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ));
  }

  _selectStartDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(), // Refer step 1
      firstDate: DateTime(
          DateTime.now().year - 5, DateTime.now().month, DateTime.now().day),
      lastDate: DateTime(
          DateTime.now().year + 5, DateTime.now().month, DateTime.now().day),
    );
    if (picked != null)
      setState(() {
        startDate = convertDateToFormat(picked);
        isStartDateSelected = true;
      });
  }

  _selectEndDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(), // Refer step 1
      firstDate: DateTime(
          DateTime.now().year - 5, DateTime.now().month, DateTime.now().day),
      lastDate: DateTime(
          DateTime.now().year + 5, DateTime.now().month, DateTime.now().day),
    );
    if (picked != null)
      setState(() {
        endDate = convertDateToFormat(picked);
        isEndDateSelected = true;
      });
  }

  _pickFile() async {
    FilePickerResult? result = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowMultiple: false,
      allowedExtensions: ['xls', 'xlsx'],
    );
    if (result != null) {
      setState(() {
        file = result.files.single.bytes;
        PlatformFile platFormFile = result.files.first;
        fileName = platFormFile.name;
      });
    } else {
      setState(() {
        fileName = "แนบไฟล์";
        file = null;
      });
    }
  }

  Future<List<RawData>?> _readExcelFile(Uint8List file) async {
    var excel = Excel.decodeBytes(file);
    var sheet = excel.tables["template"];
    if (sheet == null) {
      print("Invalid template file");
      return null;
    } else {
      int maxRow = sheet.maxRows;

      List<RawTemplates> _templates = List<RawTemplates>.empty(growable: true);
      List<String> _customerCodes = List<String>.empty(growable: true);

      for (int rowIndex = 2; rowIndex < maxRow; rowIndex++) {
        var shopCode = getCellValue(sheet, rowIndex, 9);
        var assetCode = getCellValue(sheet, rowIndex, 18);
        if (shopCode.isEmpty || shopCode == "null") {
          err = "รหัสร้านค้าไม่สามารถเว้นว่างได้";
          return null;
        } else if (assetCode.isEmpty || assetCode == "null") {
          err = "ชื่อสินค้าไม่สามารถเว้นว่างได้";
          return null;
        } else if (!isCorrectDateFormat(getCellValue(sheet, rowIndex, 8))) {
          err = "ฟอร์แมตวันที่ไม่ถูกต้อง";
          return null;
        } else if (!isCorrectYear(getCellValue(sheet, rowIndex, 8))) {
          err = "ต้องระบุปีเป็น ค.ศ. เช่น 2021 เป็นต้น";
          return null;
        } else {
          if (getCellValue(sheet, rowIndex, 1) != null &&
              getCellValue(sheet, rowIndex, 1) != "null") {
            _templates.add(RawTemplates(
              region:
                  getCellValue(sheet, rowIndex, 1).replaceAll("ภาค", "").trim(),
              companyName: getCellValue(sheet, rowIndex, 2),
              department: getCellValue(sheet, rowIndex, 6),
              area: getCellValue(sheet, rowIndex, 3),
              center: getCellValue(sheet, rowIndex, 4),
              province: getCellValue(sheet, rowIndex, 5),
              channel: getCellValue(sheet, rowIndex, 7),
              planDate: getCellValue(sheet, rowIndex, 8),
              customerCode: getCellValue(sheet, rowIndex, 9),
              customerName: getCellValue(sheet, rowIndex, 10),
              address: getCellValue(sheet, rowIndex, 11),
              location: getCellValue(sheet, rowIndex, 12),
              employeeId: getCellValue(sheet, rowIndex, 13),
              employeeName: getCellValue(sheet, rowIndex, 14),
              accountDate: getCellValue(sheet, rowIndex, 15),
              assetType: getCellValue(sheet, rowIndex, 16),
              assetCode: getCellValue(sheet, rowIndex, 17),
              assetName: getCellValue(sheet, rowIndex, 18),
              assetStatus: getCellValue(sheet, rowIndex, 19),
              unitCount: getCellValue(sheet, rowIndex, 20),
              unitPrice: getCellValue(sheet, rowIndex, 21),
              accountAmount: getCellValue(sheet, rowIndex, 22),
            ));
          }
          if (!_customerCodes.contains(getCellValue(sheet, rowIndex, 9))) {
            _customerCodes.add(getCellValue(sheet, rowIndex, 9));
          }
        }
      }
      return groupCustomerAsset(_customerCodes, _templates);
    }
  }

  bool isCorrectYear(String date) {
    try {
      var dt = DateTime.parse(date);
      var current = DateTime.now();
      if (dt.year - current.year > 2) {
        return false;
      }
      return true;
    } catch (e) {
      return false;
    }
  }

  bool isCorrectDateFormat(String date) {
    try {
      var dt = DateTime.parse(date);
      return true;
    } catch (e) {
      return false;
    }
  }

  List<RawData> groupCustomerAsset(
      List<String> _customerCodes, List<RawTemplates> _templates) {
    List<RawData> rawData = List<RawData>.empty(growable: true);

    for (var customerCode in _customerCodes) {
      List<RawTemplates> tempAsset = List<RawTemplates>.empty(growable: true);
      for (var asset in _templates) {
        if (asset.customerCode == customerCode) {
          tempAsset.add(asset);
        }
      }
      rawData.add(RawData(shopId: customerCode, rawTemplates: tempAsset));
    }
    return rawData;
  }

  String getCellValue(Sheet sheet, row, column) {
    return sheet
        .cell(CellIndex.indexByColumnRow(columnIndex: column, rowIndex: row))
        .value
        .toString()
        .trim();
  }

  _save() async {
    CoolAlert.show(
      context: context,
      type: CoolAlertType.loading,
      text: "กำลังบันทึก...",
      width: 500.0,
    );
    List<RawData>? customerAssets = await _readExcelFile(file!);
    if (customerAssets == null) {
      Navigator.pop(context);
      _showAlertError("Template ไม่ถูกต้อง : " + err);
    } else {
      CreateTemplateRequest request = CreateTemplateRequest(
          periodName: name,
          region: widget.loginResponse!.avaliableRegion,
          startDate: convertDateToRequestFormat(startDate!),
          endDate: convertDateToRequestFormat(endDate!),
          rawData: customerAssets);
      ApiResponse response = await NetworkService().createPeriod(request);
      Navigator.pop(context);
      if (response.status == NetworkService.SUCCESS) {
        _showAlertComplete();
      } else {
        _showAlertError(response.message);
      }
    }
  }

  _showAlertComplete() {
    AwesomeDialog(
      context: context,
      dialogType: DialogType.success,
      animType: AnimType.bottomSlide,
      dismissOnTouchOutside: false,
      dismissOnBackKeyPress: false,
      title: 'บันทึกสำเร็จ',
      desc: '',
      width: 500,
      btnOkText: 'ตกลง',
      btnOkColor: Colors.green,
      btnOkOnPress: () {
        Navigator.pop(context);
      },
    )..show();
  }

  _showAlertError(String? error) {
    AwesomeDialog(
      context: context,
      dialogType: DialogType.error,
      animType: AnimType.bottomSlide,
      dismissOnTouchOutside: false,
      title: 'เกิดข้อผิดพลาด',
      desc: error,
      width: 500,
      btnOkText: 'ปิด',
      btnOkColor: Colors.redAccent,
      btnOkOnPress: () {
        //dismiss
      },
    )..show();
  }
}
