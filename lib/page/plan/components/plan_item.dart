import 'package:flutter/material.dart';
import 'package:mai_confirm_asset/models/plan_response.dart';
import 'package:mai_confirm_asset/services/api_client.dart';
import 'package:mai_confirm_asset/utils/date_utils.dart';
import 'package:mai_confirm_asset/utils/home_page_app_theme.dart';
import 'package:mai_confirm_asset/utils/constants.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:url_launcher/url_launcher_string.dart';

import '../../../main.dart';

class PlanItem extends StatelessWidget {
  final Function onItemClick;
  final Periods? periods;

  const PlanItem({Key? key, required this.onItemClick, this.periods})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.transparent,
      body: Container(
        margin: const EdgeInsets.only(top: 10, right: 20, bottom: 10, left: 20),
        child: new GestureDetector(
          onTap: onItemClick as void Function()?,
          child: Container(
            decoration: BoxDecoration(
              color: HomePageAppTheme.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(8.0),
                  bottomLeft: Radius.circular(8.0),
                  bottomRight: Radius.circular(8.0),
                  topRight: Radius.circular(68.0)),
              boxShadow: <BoxShadow>[
                BoxShadow(
                    color: HomePageAppTheme.grey.withOpacity(0.2),
                    offset: Offset(1.1, 1.1),
                    blurRadius: 10.0),
              ],
            ),
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 16, left: 16, right: 16),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Padding(
                          padding:
                              const EdgeInsets.only(left: 8, right: 8, top: 4),
                          child: Column(
                            children: <Widget>[
                              _setStartMile(),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(left: 10, right: 10, bottom: 10),
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          InkWell(
                            onTap: () {
                              //launchUrlString('https://www.google.co.th');
                              launchUrlString(ApiClient.DOWNLOAD_PDF_REPORT +
                                  "periodId=" +
                                  periods!.periodId.toString());
                            },
                            // onTap: () {
                            //   AlertDialog alert = AlertDialog(
                            //     title: Text(periods.periodNName),
                            //   );
                            //   showDialog(
                            //     context: context,
                            //     builder: (BuildContext context) {
                            //       return alert;
                            //     },
                            //   );
                            // },
                            hoverColor: Colors.transparent,
                            child: Padding(
                              padding: EdgeInsets.all(5),
                              child: Container(
                                decoration: BoxDecoration(
                                    color: Color.fromARGB(255, 60, 206, 47),
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(5.0),
                                      topRight: Radius.circular(5.0),
                                      bottomLeft: Radius.circular(5.0),
                                      bottomRight: Radius.circular(5.0),
                                    )),
                                child: Padding(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 20, vertical: 10),
                                  child: Text(
                                    "ดาวน์โหลดหนังสือยืนยันยอด",
                                    style: TextStyle(
                                      color: kWhite,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 14,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      left: 24, right: 24, top: 8, bottom: 8),
                  child: Container(
                    height: 2,
                    decoration: BoxDecoration(
                      color: HomePageAppTheme.background,
                      borderRadius: BorderRadius.all(Radius.circular(4.0)),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      left: 24, right: 24, top: 0, bottom: 20),
                  child: Row(
                    children: <Widget>[
                      _setStartDate(),
                      _setEndDate(),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _setStartMile() {
    return Row(
      children: <Widget>[
        Container(
          height: 50,
          width: 2,
          decoration: BoxDecoration(
            color: HexColor('#87A0E5').withOpacity(0.2),
            borderRadius: BorderRadius.all(Radius.circular(4.0)),
          ),
        ),
        Container(
          padding: const EdgeInsets.only(left: 14),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 4, bottom: 2),
                child: Text(
                  'แผนงานเลขที่ ' + periods!.periodId.toString(),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: HomePageAppTheme.fontName,
                    fontWeight: FontWeight.w500,
                    fontSize: 14,
                    letterSpacing: -0.1,
                    color: HomePageAppTheme.grey.withOpacity(0.5),
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    width: 28,
                    height: 28,
                    child: Icon(
                      Icons.calendar_today,
                      color: HexColor('#87A0E5').withOpacity(0.5),
                    ), //Image.asset("assets/images/eaten.png"),
                  ),
                  Container(
                    margin: const EdgeInsets.only(left: 10),
                    width: 150,
                    child: Text(
                      periods!.periodNName!,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                      textAlign: TextAlign.start,
                      style: new TextStyle(
                        fontFamily: HomePageAppTheme.fontName,
                        fontWeight: FontWeight.w600,
                        fontSize: 14,
                        color: HomePageAppTheme.darkerText,
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 20)
            ],
          ),
        ),
      ],
    );
  }

  Widget _setStartDate() {
    return Expanded(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 6),
            child: Text(
              'วันที่เริ่มต้น',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontFamily: HomePageAppTheme.fontName,
                fontWeight: FontWeight.w600,
                fontSize: 12,
                color: HomePageAppTheme.grey.withOpacity(0.5),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 4),
            child: Container(
              height: 4,
              width: 70,
              decoration: BoxDecoration(
                color: HexColor('#87A0E5').withOpacity(0.2),
                borderRadius: BorderRadius.all(Radius.circular(4.0)),
              ),
              child: Row(
                children: <Widget>[
                  Container(
                    width: 70,
                    height: 4,
                    decoration: BoxDecoration(
                      gradient: LinearGradient(colors: [
                        HexColor('#87A0E5'),
                        HexColor('#87A0E5').withOpacity(0.5),
                      ]),
                      borderRadius: BorderRadius.all(Radius.circular(4.0)),
                    ),
                  )
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 4),
            child: Text(
              convertDate(periods!.startDate!),
              textAlign: TextAlign.center,
              style: TextStyle(
                fontFamily: HomePageAppTheme.fontName,
                fontWeight: FontWeight.w500,
                fontSize: 14,
                letterSpacing: -0.2,
                color: HomePageAppTheme.darkText.withOpacity(0.5),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _setEndDate() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 6),
              child: Text(
                'วันที่สิ้นสุด',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontFamily: HomePageAppTheme.fontName,
                  fontWeight: FontWeight.w600,
                  fontSize: 12,
                  color: HomePageAppTheme.grey.withOpacity(0.5),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(right: 0, top: 4),
              child: Container(
                height: 4,
                width: 70,
                decoration: BoxDecoration(
                  color: HexColor('#F56E98').withOpacity(0.2),
                  borderRadius: BorderRadius.all(Radius.circular(4.0)),
                ),
                child: Row(
                  children: <Widget>[
                    Container(
                      width: 70,
                      height: 4,
                      decoration: BoxDecoration(
                        gradient: LinearGradient(colors: [
                          HexColor('#F56E98').withOpacity(0.1),
                          HexColor('#F56E98'),
                        ]),
                        borderRadius: BorderRadius.all(Radius.circular(4.0)),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(right: 0, top: 4),
              child: Text(
                convertDate(periods!.endDate!),
                style: TextStyle(
                  fontFamily: HomePageAppTheme.fontName,
                  fontWeight: FontWeight.w500,
                  fontSize: 14,
                  letterSpacing: -0.2,
                  color: HomePageAppTheme.darkText.withOpacity(0.5),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
