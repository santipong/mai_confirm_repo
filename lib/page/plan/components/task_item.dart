import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:mai_confirm_asset/models/task_response.dart';
import 'package:mai_confirm_asset/utils/constants.dart';
import 'package:mai_confirm_asset/utils/date_utils.dart';

class TaskItem extends StatelessWidget {
  final Function? onDeletePress, onEditPress;
  final Tasks? tasks;

  const TaskItem({
    Key? key,
    this.onDeletePress,
    this.tasks,
    this.onEditPress,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {},
      child: Container(
        width: 600,
        child: Column(
          children: <Widget>[
            Padding(
              padding:
                  const EdgeInsets.only(left: 10, right: 10, top: 0, bottom: 0),
              child: Stack(
                clipBehavior: Clip.none,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 5, bottom: 5),
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(8.0),
                            bottomLeft: Radius.circular(8.0),
                            bottomRight: Radius.circular(8.0),
                            topRight: Radius.circular(8.0)),
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                              color: Colors.grey.withOpacity(0.4),
                              offset: Offset(1.1, 1.1),
                              blurRadius: 10.0),
                        ],
                      ),
                      child: Stack(
                        alignment: Alignment.topLeft,
                        children: <Widget>[
                          ClipRRect(
                            borderRadius:
                                BorderRadius.all(Radius.circular(8.0)),
                            child: SizedBox(
                              height: 74,
                            ),
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      left: 16,
                                      right: 16,
                                      top: 25,
                                    ),
                                    child: Text(
                                      "ร้านค้า : " + tasks!.taskName!,
                                      overflow: TextOverflow.ellipsis,
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                        fontWeight: FontWeight.w700,
                                        fontSize: 16,
                                        letterSpacing: 0.0,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  left: 16,
                                  bottom: 10,
                                  right: 16,
                                ),
                              ),
                              Row(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      left: 16,
                                      right: 16,
                                    ),
                                    child: Text(
                                      "ที่อยู่ : " + tasks!.shop!.address!,
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        fontSize: 16,
                                        letterSpacing: 0.0,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  left: 100,
                                  bottom: 10,
                                  right: 16,
                                ),
                              ),
                              Row(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      left: 16,
                                      right: 16,
                                    ),
                                    child: Text(
                                      getPlanDate(),
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        fontSize: 16,
                                        letterSpacing: 0.0,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  left: 100,
                                  bottom: 10,
                                  right: 16,
                                ),
                              ),
                              Row(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      left: 16,
                                      right: 16,
                                    ),
                                    child: Text(
                                      getCheckDate(),
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        fontSize: 16,
                                        letterSpacing: 0.0,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  left: 16,
                                  bottom: 10,
                                  right: 16,
                                ),
                              ),
                              Row(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      left: 16,
                                    ),
                                    child: Text(
                                      "สถานะ : ",
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        fontSize: 16,
                                        letterSpacing: 0.0,
                                      ),
                                    ),
                                  ),
                                  Padding(
                                      padding: const EdgeInsets.only(
                                        right: 16,
                                      ),
                                      child: getShopStatus()),
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  left: 16,
                                  bottom: 10,
                                  right: 16,
                                ),
                              ),
                              Row(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      left: 16,
                                    ),
                                    child: editButton(context),
                                  ),
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  left: 16,
                                  bottom: 12,
                                  top: 4,
                                  right: 16,
                                ),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  String getCheckDate() {
    if (tasks!.checkShop!.shopStatus == "unchecked") {
      return "วันที่ตรวจนับ : -";
    }
    return "วันที่ตรวจนับ : " + convertDate(tasks!.checkDate!);
  }

  String getPlanDate() {
    return "วันที่ตามแผน : " + convertDate(tasks!.planDate!);
  }

  Widget getShopStatus() {
    switch (tasks!.checkShop!.shopStatus) {
      case "open":
        return Text(
          "ร้านเปิด",
          textAlign: TextAlign.left,
          style: TextStyle(
            color: Colors.greenAccent,
            fontWeight: FontWeight.w400,
            fontSize: 16,
            letterSpacing: 0.0,
          ),
        );
      case "close":
        return Text(
          "ร้านปิด",
          textAlign: TextAlign.left,
          style: TextStyle(
            color: Colors.redAccent,
            fontWeight: FontWeight.w400,
            fontSize: 16,
            letterSpacing: 0.0,
          ),
        );
      case "go-out-of-busines":
        return Text(
          "เลิกกิจการ",
          textAlign: TextAlign.left,
          style: TextStyle(
            color: Colors.orangeAccent.shade400,
            fontWeight: FontWeight.w400,
            fontSize: 16,
            letterSpacing: 0.0,
          ),
        );
      case "not-found":
        return Text(
          "ไม่พบร้าน",
          textAlign: TextAlign.left,
          style: TextStyle(
            fontWeight: FontWeight.w400,
            fontSize: 16,
            letterSpacing: 0.0,
          ),
        );
      default:
        return Text(
          "ยังไม่เข้าตรวจ",
          textAlign: TextAlign.left,
          style: TextStyle(
            fontWeight: FontWeight.w400,
            fontSize: 16,
            letterSpacing: 0.0,
          ),
        );
    }
  }

  Widget editButton(context) {
    return Align(
      alignment: Alignment.bottomRight,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          InkWell(
            onTap: () {
              onEditPress!.call();
            },
            hoverColor: Colors.transparent,
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 5),
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.blueAccent,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(5.0),
                      topRight: Radius.circular(5.0),
                      bottomLeft: Radius.circular(5.0),
                      bottomRight: Radius.circular(5.0),
                    )),
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                  child: Text(
                    "แก้ไข",
                    style: TextStyle(
                      color: kWhite,
                      fontWeight: FontWeight.w600,
                      fontSize: 12,
                    ),
                  ),
                ),
              ),
            ),
          ),
          InkWell(
            onTap: () {
              AwesomeDialog(
                context: context,
                dialogType: DialogType.info,
                animType: AnimType.bottomSlide,
                width: 500,
                title: 'ยืนยันการลบข้อมูล',
                desc: 'คุณต้องการลบข้อมูลร้านค้า ' +
                    tasks!.taskName! +
                    ' เมื่อลบแล้วจะไม่สามารถนำข้อมูลกลับมาได้ ยืนยัน หรือยกเลิก ?',
                btnOkText: "ยืนยัน",
                btnCancelText: "ยกเลิก",
                btnCancelOnPress: () {},
                btnOkOnPress: () {
                  onDeletePress!.call();
                },
              )..show();
            },
            hoverColor: Colors.transparent,
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 5),
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.redAccent,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(5.0),
                      topRight: Radius.circular(5.0),
                      bottomLeft: Radius.circular(5.0),
                      bottomRight: Radius.circular(5.0),
                    )),
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                  child: Text(
                    "ลบ",
                    style: TextStyle(
                      color: kWhite,
                      fontWeight: FontWeight.w600,
                      fontSize: 12,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
