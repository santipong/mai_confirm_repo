import 'dart:ui';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:cool_alert/cool_alert.dart';
import 'package:excel/excel.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mai_confirm_asset/models/edit_shop_request.dart';
import 'package:mai_confirm_asset/models/request_create_template.dart';
import 'package:mai_confirm_asset/models/response.dart';
import 'package:mai_confirm_asset/models/task_response.dart';
import 'package:mai_confirm_asset/services/network_service.dart';
import 'package:mai_confirm_asset/utils/constants.dart';

class EditShopDialog extends StatefulWidget {
  final String? title, descriptions, text;
  final Tasks? tasks;
  final Image? img;

  const EditShopDialog({
    Key? key,
    this.title,
    this.descriptions,
    this.text,
    this.img,
    this.tasks,
  }) : super(key: key);

  @override
  _EditShopDialogState createState() => _EditShopDialogState();
}

class _EditShopDialogState extends State<EditShopDialog> {
  final _formKey = GlobalKey<FormState>();
  String? fileName, filePath;
  PlatformFile? file;
  bool isFirstImage = false;
  bool initData = false;
  bool editable = true;

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(kPadding),
      ),
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: contentBox(context),
    );
  }

  contentBox(context) {
    return Stack(
      children: <Widget>[
        Container(
          width: 600,
          padding: EdgeInsets.only(
              left: kPadding,
              top: kAvatarRadius + kPadding,
              right: kPadding,
              bottom: kAvatarRadius),
          margin: EdgeInsets.only(top: kAvatarRadius),
          decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              color: Colors.white,
              borderRadius: BorderRadius.circular(kPadding),
              boxShadow: [
                BoxShadow(
                    color: Colors.black26,
                    offset: Offset(0, 10),
                    blurRadius: 10),
              ]),
          child: SafeArea(
            child: Scrollbar(
              child: SingleChildScrollView(
                child: Form(
                  key: _formKey,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Text(
                        widget.title!,
                        style: TextStyle(
                            fontSize: 22, fontWeight: FontWeight.w600),
                      ),
                      SizedBox(height: 15),
                      Text(
                        widget.descriptions!,
                        style: TextStyle(fontSize: 14),
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(height: 22),
                      _form(),
                      SizedBox(height: 22),
                      Align(
                          alignment: Alignment.bottomRight,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              InkWell(
                                onTap: () {
                                  Navigator.pop(context);
                                },
                                hoverColor: Colors.transparent,
                                child: Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 5),
                                  child: Container(
                                    decoration: BoxDecoration(
                                        color: Colors.redAccent,
                                        borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(5.0),
                                          topRight: Radius.circular(5.0),
                                          bottomLeft: Radius.circular(5.0),
                                          bottomRight: Radius.circular(5.0),
                                        )),
                                    child: Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 15, vertical: 10),
                                      child: Text(
                                        "ยกเลิก",
                                        style: TextStyle(
                                          color: kWhite,
                                          fontWeight: FontWeight.w600,
                                          fontSize: 16,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              InkWell(
                                onTap: () {
                                  if (_formKey.currentState!.validate()) {
                                    if (file == null) {
                                      _showAlertError("กรุณาแนบไฟล์");
                                    } else {
                                      _formKey.currentState!.save();
                                      _save();
                                    }
                                  }
                                },
                                hoverColor: Colors.transparent,
                                child: Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 5),
                                  child: Container(
                                    decoration: BoxDecoration(
                                        color: Colors.green,
                                        borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(5.0),
                                          topRight: Radius.circular(5.0),
                                          bottomLeft: Radius.circular(5.0),
                                          bottomRight: Radius.circular(5.0),
                                        )),
                                    child: Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 15, vertical: 10),
                                      child: Text(
                                        "บันทึก",
                                        style: TextStyle(
                                          color: kWhite,
                                          fontWeight: FontWeight.w600,
                                          fontSize: 16,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          )),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _form() {
    return Column(
      children: [
        SizedBox(height: 30),
        _buildFilePicker(),
        SizedBox(height: 30),
      ],
    );
  }

  List<String> splitStringByLength(String str, int length) =>
      [str.substring(0, length), str.substring(length)];

  String splitString(String name) {
    if (name.length <= 40) {
      return name;
    }
    return splitStringByLength(name, 37)[0] + "...";
  }

  Widget _buildFilePicker() {
    return SizedBox(
        width: 420,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Text(
              splitString(fileName ?? "ไฟล์แนบ"),
              style: TextStyle(fontSize: 14, color: Colors.black87),
            ),
            InkWell(
              onTap: () {
                _pickFile();
              },
              hoverColor: Colors.transparent,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 15),
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.blueGrey,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey,
                          blurRadius: 6,
                          offset: Offset(0, 5),
                        ),
                      ],
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(5.0),
                        topRight: Radius.circular(5.0),
                        bottomLeft: Radius.circular(5.0),
                        bottomRight: Radius.circular(5.0),
                      )),
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 25, vertical: 7),
                    child: Text(
                      "เลือกไฟล์แนบ",
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w300,
                        fontSize: 14,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ));
  }

  _pickFile() async {
    FilePickerResult? result = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowMultiple: false,
      allowedExtensions: ['xls', 'xlsx'],
    );

    if (result != null) {
      setState(() {
        file = result.files.first;
        fileName = file!.name;
      });
    } else {
      setState(() {
        fileName = "แนบไฟล์";
        file = null;
      });
    }
  }

  Future<List<RawData>?> _readExcelFile(PlatformFile file) async {
    var excel = Excel.decodeBytes(file.bytes!);
    var sheet = excel.tables["template"];
    if (sheet == null) {
      print("Invalid template file");
      return null;
    } else {
      int maxRow = sheet.maxRows;

      List<RawTemplates> _templates = List<RawTemplates>.empty(growable: true);
      List<String> _customerCodes = List<String>.empty(growable: true);

      for (int rowIndex = 2; rowIndex < maxRow; rowIndex++) {
        _templates.add(RawTemplates(
          region: getCellValue(sheet, rowIndex, 1),
          companyName: getCellValue(sheet, rowIndex, 2),
          department: getCellValue(sheet, rowIndex, 6),
          area: getCellValue(sheet, rowIndex, 3),
          center: getCellValue(sheet, rowIndex, 4),
          province: getCellValue(sheet, rowIndex, 5),
          channel: getCellValue(sheet, rowIndex, 7),
          planDate: getCellValue(sheet, rowIndex, 8),
          customerCode: getCellValue(sheet, rowIndex, 9),
          customerName: getCellValue(sheet, rowIndex, 10),
          address: getCellValue(sheet, rowIndex, 11),
          location: getCellValue(sheet, rowIndex, 12),
          employeeId: getCellValue(sheet, rowIndex, 13),
          employeeName: getCellValue(sheet, rowIndex, 14),
          accountDate: getCellValue(sheet, rowIndex, 15),
          assetType: getCellValue(sheet, rowIndex, 16),
          assetCode: getCellValue(sheet, rowIndex, 17),
          assetName: getCellValue(sheet, rowIndex, 18),
          assetStatus: getCellValue(sheet, rowIndex, 19),
          unitCount: getCellValue(sheet, rowIndex, 20),
          unitPrice: getCellValue(sheet, rowIndex, 21),
          accountAmount: getCellValue(sheet, rowIndex, 22),
        ));
        if (!_customerCodes.contains(getCellValue(sheet, rowIndex, 9))) {
          _customerCodes.add(getCellValue(sheet, rowIndex, 9));
        }
      }
      return groupCustomerAsset(_customerCodes, _templates);
    }
  }

  List<RawData> groupCustomerAsset(
      List<String> _customerCodes, List<RawTemplates> _templates) {
    List<RawData> rawData = List<RawData>.empty(growable: true);

    for (var customerCode in _customerCodes) {
      List<RawTemplates> tempAsset = List<RawTemplates>.empty(growable: true);
      for (var asset in _templates) {
        if (asset.customerCode == customerCode) {
          tempAsset.add(asset);
        }
      }
      rawData.add(RawData(shopId: customerCode, rawTemplates: tempAsset));
    }
    return rawData;
  }

  String getCellValue(Sheet sheet, row, column) {
    return sheet
        .cell(CellIndex.indexByColumnRow(columnIndex: column, rowIndex: row))
        .value
        .toString();
  }

  _save() async {
    CoolAlert.show(
      context: context,
      type: CoolAlertType.loading,
      text: "กำลังบันทึก...",
      width: 500.0,
    );
    List<RawData>? customerAssets = await _readExcelFile(file!);
    if (customerAssets == null) {
      Navigator.pop(context);
      _showAlertError("Template ไม่ถูกต้อง");
    } else {
      EditShopRequest request = EditShopRequest(rawData: customerAssets);
      ApiResponse response = await NetworkService()
          .editShop(request, widget.tasks!.taskID.toString());
      Navigator.pop(context);
      if (response.status == NetworkService.SUCCESS) {
        _showAlertComplete();
      } else {
        _showAlertError(response.message);
      }
    }
  }

  _showAlertComplete() {
    AwesomeDialog(
      context: context,
      dialogType: DialogType.success,
      animType: AnimType.bottomSlide,
      dismissOnTouchOutside: false,
      dismissOnBackKeyPress: false,
      title: 'บันทึกสำเร็จ',
      desc: '',
      width: 500,
      btnOkText: 'ตกลง',
      btnOkColor: Colors.green,
      btnOkOnPress: () {
        Navigator.pop(context);
      },
    )..show();
  }

  _showAlertError(String? error) {
    AwesomeDialog(
      context: context,
      dialogType: DialogType.error,
      animType: AnimType.bottomSlide,
      dismissOnTouchOutside: false,
      title: 'เกิดข้อผิดพลาด',
      desc: error,
      width: 500,
      btnOkText: 'ปิด',
      btnOkColor: Colors.redAccent,
      btnOkOnPress: () {
        //dismiss
      },
    )..show();
  }
}
