import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:lottie/lottie.dart';
import 'package:mai_confirm_asset/models/login_response.dart';
import 'package:mai_confirm_asset/models/plan_response.dart';
import 'package:mai_confirm_asset/page/plan/components/create_dialog.dart';
import 'package:mai_confirm_asset/page/plan/components/plan_item.dart';
import 'package:mai_confirm_asset/page/plan/dialog_plan_detail.dart';
import 'package:mai_confirm_asset/services/api_client.dart';
import 'package:mai_confirm_asset/services/network_service.dart';
import 'package:mai_confirm_asset/utils/constants.dart';
import 'package:mai_confirm_asset/utils/home_page_app_theme.dart';
import 'package:mai_confirm_asset/utils/responsive.dart';

class CreatePlanForm extends StatefulWidget {
  final LoginResponse? loginResponse;

  const CreatePlanForm({
    Key? key,
    this.loginResponse,
  }) : super(key: key);

  @override
  _CreatePlanFormState createState() => _CreatePlanFormState();
}

class _CreatePlanFormState extends State<CreatePlanForm> {
  @override
  Widget build(BuildContext context) {
    int rowCount = 0;
    var cardHeight = 300.0;
    if (isMobile(context)) {
      rowCount = 1;
    } else if (isTab(context)) {
      rowCount = 2;
    } else {
      rowCount = 4;
    }

    return FutureBuilder<PlanResponse>(
      future: NetworkService().getPlan(widget.loginResponse!.avaliableRegion!),
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return buildError(context, rowCount);
        }
        if (snapshot.hasData) {
          final planResponse = snapshot.data!;
          if (planResponse.status == ApiClient.STATUS_SUCCESS) {
            return buildSuccess(context, rowCount, planResponse);
          } else {
            return buildError(context, rowCount);
          }
        }
        return buildLoading();
      },
    );
  }

  Widget buildLoading() {
    return Center(
      child: Padding(
          padding: EdgeInsets.all(10.0), child: CircularProgressIndicator()),
    );
  }

  Widget buildError(context, int rowCount) {
    var _crossAxisSpacing = 10.0;
    var _screenWidth = MediaQuery.of(context).size.width;
    var _crossAxisCount = rowCount;
    var _width = (_screenWidth - ((_crossAxisCount - 1) * _crossAxisSpacing)) /
        _crossAxisCount;
    var cellHeight = 250;
    var _aspectRatio = _width / cellHeight;

    return Expanded(
      child: Container(
        decoration: BoxDecoration(color: kGrayBg),
        child: GridView.count(
          primary: false,
          padding: const EdgeInsets.all(20),
          childAspectRatio: _aspectRatio,
          crossAxisSpacing: _crossAxisSpacing,
          mainAxisSpacing: 10,
          crossAxisCount: rowCount,
          children: <Widget>[
            createPlanRound(),
          ],
        ),
      ),
    );
  }

  Widget buildSuccess(context, int rowCount, PlanResponse response) {
    var _crossAxisSpacing = 10.0;
    var _screenWidth = MediaQuery.of(context).size.width;
    var _crossAxisCount = rowCount;
    var _width = (_screenWidth - ((_crossAxisCount - 1) * _crossAxisSpacing)) /
        _crossAxisCount;
    var cellHeight = 290;
    var _aspectRatio = _width / cellHeight;

    var planCards = List<Widget>.empty(growable: true);
    planCards.add(createPlanRound());

    for (var period in response.periods!) {
      planCards.add(planCard(context, period));
    }

    return Expanded(
      child: Container(
        decoration: BoxDecoration(color: kGrayBg),
        child: GridView.count(
          primary: false,
          padding: const EdgeInsets.all(20),
          childAspectRatio: _aspectRatio,
          crossAxisSpacing: _crossAxisSpacing,
          mainAxisSpacing: 10,
          crossAxisCount: _crossAxisCount,
          scrollDirection: Axis.vertical,
          children: planCards,
        ),
      ),
    );
  }

  Widget createPlanRound() {
    return InkWell(
      onTap: () {
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return CustomDialogBox(
                title: "สร้างแผนงานใหม่",
                descriptions:
                    "กรอกข้อมูลแผนงานที่คุณต้องการสร้าง และอัปโหลดไฟล์ตาม Template",
                text: "บันทึก",
                loginResponse: widget.loginResponse,
              );
            }).then((val) {
          setState(() {});
        });
      },
      child: Padding(
        padding:
            const EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 10),
        child: Container(
            height: 100.0,
            decoration: BoxDecoration(
              color: HomePageAppTheme.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(8.0),
                  bottomLeft: Radius.circular(8.0),
                  bottomRight: Radius.circular(8.0),
                  topRight: Radius.circular(68.0)),
              boxShadow: <BoxShadow>[
                BoxShadow(
                    color: HomePageAppTheme.grey.withOpacity(0.2),
                    offset: Offset(1.1, 1.1),
                    blurRadius: 10.0),
              ],
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      "สร้างแผน",
                      style: TextStyle(fontSize: 20),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8, right: 8, top: 4),
                      child: Align(
                        alignment: Alignment.center,
                        child: Padding(
                          padding: EdgeInsets.all(10),
                          child: SizedBox(
                            width: 70,
                            height: 70,
                            child: Lottie.network(
                                'https://assets10.lottiefiles.com/packages/lf20_bzhzelwb.json'),
                          ),
                        ),
                      ),
                    ),
                  ],
                )
              ],
            )),
      ),
    );
  }

  Widget planCard(context, Periods periods) {
    return PlanItem(
      periods: periods,
      onItemClick: () {
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return PlanDetailDialog(
                periodId: periods.periodId.toString(),
                title: "ข้อมูลแผนงาน",
                descriptions: periods.periodNName,
                text: "แก้ไขแผน",
                periods: periods,
                loginResponse: widget.loginResponse,
              );
            }).then((val) {
          setState(() {});
        });
      },
    );
  }
}
