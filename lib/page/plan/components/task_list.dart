import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:cool_alert/cool_alert.dart';
import 'package:flutter/material.dart';
import 'package:mai_confirm_asset/models/response.dart';
import 'package:mai_confirm_asset/models/task_response.dart';
import 'package:mai_confirm_asset/page/plan/components/edit_shop_dialog.dart';
import 'package:mai_confirm_asset/page/plan/components/task_item.dart';
import 'package:mai_confirm_asset/services/api_client.dart';
import 'package:mai_confirm_asset/services/network_service.dart';

class TaskListPage extends StatefulWidget {
  final String? periodId;
  TaskListPage({
    Key? key,
    this.periodId,
  }) : super(key: key);

  @override
  _TaskListPageState createState() => _TaskListPageState();
}

class _TaskListPageState extends State<TaskListPage> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<TaskResponse>(
      future: NetworkService().getTask(widget.periodId!),
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return buildError(snapshot.error.toString());
        }
        if (snapshot.hasData) {
          final taskResponse = snapshot.data!;
          if (taskResponse.status == ApiClient.STATUS_SUCCESS) {
            return buildSuccess(context, taskResponse);
          } else {
            return buildError(taskResponse.message!);
          }
        }
        return buildLoading();
      },
    );
  }

  Widget buildLoading() {
    return Center(
      child: Padding(
          padding: EdgeInsets.all(10.0), child: CircularProgressIndicator()),
    );
  }

  Widget buildError(String message) {
    return Center(
      child: Text(message),
    );
  }

  Widget buildSuccess(context, TaskResponse taskResponse) {
    var widgets = new List<Widget>.empty(growable: true);
    widgets.add(Padding(
        padding: const EdgeInsets.only(bottom: 10),
        child: Container(
          width: 550,
          alignment: Alignment.centerLeft,
          child: Text(
            "จำนวนร้านทั้งหมด " +
                taskResponse.tasks!.length.toString() +
                " ร้าน",
            style: TextStyle(fontSize: 14, fontWeight: FontWeight.w600),
            textAlign: TextAlign.start,
          ),
        )));
    for (var task in taskResponse.tasks!) {
      widgets.add(TaskItem(
        tasks: task,
        onDeletePress: () {
          deleteTask(task.taskID);
        },
        onEditPress: () {
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return EditShopDialog(
                  title: "แก้ไขแผนงานรายร้าน",
                  descriptions: "กรุณาอัปโหลดไฟล์ตาม Template",
                  text: "บันทึก",
                  tasks: task,
                );
              }).then((val) {
            setState(() {});
          });
        },
      ));
    }
    return Container(
      child: Column(
        children: widgets,
      ),
    );
  }

  Future<void> deleteTask(int? taskId) async {
    CoolAlert.show(
      context: context,
      type: CoolAlertType.loading,
      text: "กำลังลบข้อมูล...",
      width: 500.0,
    );
    ApiResponse response = await NetworkService().deleteTask(taskId.toString());
    Navigator.pop(context);
    if (response.status == ApiClient.STATUS_SUCCESS) {
      _showAlertComplete();
    } else {
      _showAlertError(response.message);
    }
    setState(() {});
  }

  _showAlertComplete() {
    AwesomeDialog(
      context: context,
      dialogType: DialogType.success,
      animType: AnimType.bottomSlide,
      dismissOnTouchOutside: false,
      dismissOnBackKeyPress: false,
      title: 'ลบสำเร็จ',
      desc: '',
      width: 500,
      btnOkText: 'ตกลง',
      btnOkColor: Colors.green,
      btnOkOnPress: () {},
    )..show();
  }

  _showAlertError(String? error) {
    AwesomeDialog(
      context: context,
      dialogType: DialogType.error,
      animType: AnimType.bottomSlide,
      dismissOnTouchOutside: false,
      title: 'เกิดข้อผิดพลาด',
      desc: error,
      width: 500,
      btnOkText: 'ปิด',
      btnOkColor: Colors.redAccent,
      btnOkOnPress: () {
        //dismiss
      },
    )..show();
  }
}
