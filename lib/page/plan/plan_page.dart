import 'dart:convert';
import 'dart:js' as js;

import 'package:floating_action_bubble/floating_action_bubble.dart';
import 'package:flutter/material.dart';
import 'package:mai_confirm_asset/main.dart';
import 'package:mai_confirm_asset/models/login_response.dart';
import 'package:mai_confirm_asset/page/components/header.dart';
import 'package:mai_confirm_asset/page/login/login_page.dart';
import 'package:mai_confirm_asset/page/plan/components/create_plan_form.dart';
import 'package:mai_confirm_asset/page/components/side_menu.dart';
import 'package:mai_confirm_asset/utils/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PlanPage extends StatefulWidget {
  @override
  _PlanPageState createState() => _PlanPageState();
}

class _PlanPageState extends State<PlanPage>
    with SingleTickerProviderStateMixin {
  late Animation<double> _animation;
  late AnimationController _animationController;
  bool isOpened = false;

  @override
  void initState() {
    _animationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 260),
    );

    final curvedAnimation =
        CurvedAnimation(curve: Curves.easeInOut, parent: _animationController);
    _animation = Tween<double>(begin: 0, end: 1).animate(curvedAnimation);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<LoginResponse>(
      future: getLoginData(),
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          Navigator.pushNamed(context, Routes.login);
        }
        if (snapshot.hasData) {
          final LoginResponse? loginResponse = snapshot.data;
          if (loginResponse != null) {
            return body(context, loginResponse);
          } else {
            Navigator.pushNamed(context, Routes.login);
          }
        }
        return buildLoading();
      },
    );
  }

  Widget buildLoading() {
    return Center(
      child: Padding(
          padding: EdgeInsets.all(10.0), child: CircularProgressIndicator()),
    );
  }

  Widget buildError() {
    return Center(
      child: Padding(
          padding: EdgeInsets.all(10.0), child: Text("ไม่พบข้อมูลผู้ใช้งาน")),
    );
  }

  Widget body(BuildContext context, LoginResponse loginResponse) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      endDrawer: ConstrainedBox(
        constraints: BoxConstraints(maxWidth: 300),
        child: SideMenu(
          currentPage: PAGE_CREATE_PLAN,
        ),
      ),
      body: SafeArea(
        child: Container(
          decoration: BoxDecoration(color: Colors.white),
          width: size.width,
          constraints: BoxConstraints(minHeight: size.height),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Header(
                currentPage: PAGE_CREATE_PLAN,
                loginResponse: loginResponse,
              ),
              loginResponse != null
                  ? CreatePlanForm(
                      loginResponse: loginResponse,
                    )
                  : buildError(),
              //Footer(),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionBubble(
        // Menu items
        items: <Bubble>[
          // Floating action menu item
          Bubble(
            title: "ไฟล์ Template",
            iconColor: Colors.white,
            bubbleColor: Colors.pinkAccent,
            icon: Icons.download,
            titleStyle: TextStyle(fontSize: 16, color: Colors.white),
            onPress: () {
              isOpened = false;
              js.context.callMethod('open', [
                'https://ss.thaibev.com/mai-confirm-asset/api/downloadTemplate'
              ]);
              _animationController.reverse();
            },
          ),
          //Floating action menu item
          Bubble(
            title: "คู่มือ",
            iconColor: Colors.white,
            bubbleColor: Colors.pinkAccent,
            icon: Icons.book,
            titleStyle: TextStyle(fontSize: 16, color: Colors.white),
            onPress: () {
              isOpened = false;
              js.context.callMethod('open', [
                'https://drive.google.com/file/d/1ODkrQbP_TCsganSwZyIYcs67tGlylpGC/view?usp=sharing'
              ]);
              _animationController.reverse();
            },
          ),
        ],
        // animation controller
        animation: _animation,

        // On pressed change animation state
        onPress: () {
          if (isOpened) {
            isOpened = false;
            _animationController.reverse();
          } else {
            isOpened = true;
            _animationController.forward();
          }
        },

        // Floating Action button Icon color
        iconColor: Colors.white,

        backGroundColor: kPrimaryColor,

        // Flaoting Action button Icon
        animatedIconData: AnimatedIcons.menu_arrow,
      ),
    );
  }

  Future<LoginResponse> getLoginData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String rawJson = prefs.getString(LoginPage.loginKey)!;
    Map<String, dynamic> json = jsonDecode(rawJson);
    return LoginResponse.fromJson(json);
  }
}
