import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:jwt_decode/jwt_decode.dart';
import 'package:mai_confirm_asset/main.dart';
import 'package:mai_confirm_asset/models/login_request.dart';
import 'package:mai_confirm_asset/models/login_response.dart';
import 'package:mai_confirm_asset/services/network_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CheckLoginPage extends StatefulWidget {
  static const String loginKey = 'login';
  final String? token;

  const CheckLoginPage({
    Key? key,
    this.token,
  }) : super(key: key);

  @override
  _CheckLoginPageState createState() => _CheckLoginPageState();
}

class _CheckLoginPageState extends State<CheckLoginPage> {
  @override
  void initState() {
    super.initState();
    getToken().then((data) {
      setState(() {
        if (data.status == "success") {
          openPlanPage(context);
        } else {
          openLoginPage(context);
        }
      });
    }).catchError((e) {
      openLoginPage(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    return buildLoading();
  }

  Widget buildLoading() {
    return Center(
      child: Padding(
          padding: EdgeInsets.all(10.0), child: CircularProgressIndicator()),
    );
  }

  void openPlanPage(BuildContext context) {
    Navigator.pushNamedAndRemoveUntil(context, Routes.createPlan, (r) => false);
  }

  void openLoginPage(BuildContext context) {
    Navigator.pushNamedAndRemoveUntil(context, Routes.login, (r) => false);
  }

  Future<LoginResponse> getToken() async {
    String? empId = _getEmployeeId(widget.token!);
    LoginRequest request = LoginRequest(userId: empId, password: empId);
    LoginResponse response = await NetworkService().login(request);
    if (response.status == NetworkService.SUCCESS) {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      await prefs.setString(CheckLoginPage.loginKey, jsonEncode(response));
    }
    return response;
  }

  String? _getEmployeeId(String token) {
    try {
      Map<String, dynamic> payload = Jwt.parseJwt(token);
      return payload["unique_name"];
    } catch (e) {
      return "";
    }
  }
}
