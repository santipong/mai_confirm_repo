class LoginRequest {
  String? userId;
  String? password;

  LoginRequest({this.userId, this.password});

  LoginRequest.fromJson(Map<String, dynamic> json) {
    userId = json['userId'];
    password = json['password'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['userId'] = this.userId;
    data['password'] = this.password;
    return data;
  }
}
