class TaskResponse {
  String? status;
  String? message;
  List<Tasks>? tasks;

  TaskResponse({this.status, this.message, this.tasks});

  TaskResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    if (json['tasks'] != null) {
      tasks = [];
      json['tasks'].forEach((v) {
        tasks!.add(new Tasks.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.tasks != null) {
      data['tasks'] = this.tasks!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Tasks {
  int? taskID;
  int? periodId;
  int? shopId;
  String? taskName;
  String? planDate;
  String? checkDate;
  CheckShop? checkShop;
  Shop? shop;
  List<KeeperDetail>? keeperDetail;

  Tasks(
      {this.taskID,
      this.periodId,
      this.shopId,
      this.taskName,
      this.planDate,
      this.checkDate,
      this.checkShop,
      this.shop,
      this.keeperDetail});

  Tasks.fromJson(Map<String, dynamic> json) {
    taskID = json['taskID'];
    periodId = json['periodId'];
    shopId = json['shopId'];
    taskName = json['taskName'];
    planDate = json['planDate'];
    checkDate = json['checkDate'];
    checkShop = json['checkShop'] != null
        ? new CheckShop.fromJson(json['checkShop'])
        : null;
    shop = json['shop'] != null ? new Shop.fromJson(json['shop']) : null;
    if (json['keeperDetail'] != null) {
      keeperDetail = [];
      json['keeperDetail'].forEach((v) {
        keeperDetail!.add(new KeeperDetail.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['taskID'] = this.taskID;
    data['periodId'] = this.periodId;
    data['shopId'] = this.shopId;
    data['taskName'] = this.taskName;
    data['planDate'] = this.planDate;
    data['checkDate'] = this.checkDate;
    if (this.checkShop != null) {
      data['checkShop'] = this.checkShop!.toJson();
    }
    if (this.shop != null) {
      data['shop'] = this.shop!.toJson();
    }
    if (this.keeperDetail != null) {
      data['keeperDetail'] = this.keeperDetail!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class CheckShop {
  int? checkShopId;
  int? taskID;
  String? shopStatus;
  String? shopImage;
  String? checkLocation;
  String? remark;

  CheckShop(
      {this.checkShopId,
      this.taskID,
      this.shopStatus,
      this.shopImage,
      this.checkLocation,
      this.remark});

  CheckShop.fromJson(Map<String, dynamic> json) {
    checkShopId = json['checkShopId'];
    taskID = json['taskID'];
    shopStatus = json['shopStatus'];
    shopImage = json['shopImage'];
    checkLocation = json['checkLocation'];
    remark = json['remark'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['checkShopId'] = this.checkShopId;
    data['taskID'] = this.taskID;
    data['shopStatus'] = this.shopStatus;
    data['shopImage'] = this.shopImage;
    data['checkLocation'] = this.checkLocation;
    data['remark'] = this.remark;
    return data;
  }
}

class Shop {
  int? shopId;
  String? shopCode;
  String? shopName;
  String? address;
  String? location;

  Shop(
      {this.shopId, this.shopCode, this.shopName, this.address, this.location});

  Shop.fromJson(Map<String, dynamic> json) {
    shopId = json['shopId'];
    shopCode = json['shopCode'];
    shopName = json['shopName'];
    address = json['address'];
    location = json['location'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['shopId'] = this.shopId;
    data['shopCode'] = this.shopCode;
    data['shopName'] = this.shopName;
    data['address'] = this.address;
    data['location'] = this.location;
    return data;
  }
}

class KeeperDetail {
  int? empId;
  String? empName;
  Department? department;
  Company? company;

  KeeperDetail({this.empId, this.empName, this.department, this.company});

  KeeperDetail.fromJson(Map<String, dynamic> json) {
    empId = json['empId'];
    empName = json['empName'];
    department = json['department'] != null
        ? new Department.fromJson(json['department'])
        : null;
    company =
        json['company'] != null ? new Company.fromJson(json['company']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['empId'] = this.empId;
    data['empName'] = this.empName;
    if (this.department != null) {
      data['department'] = this.department!.toJson();
    }
    if (this.company != null) {
      data['company'] = this.company!.toJson();
    }
    return data;
  }
}

class Department {
  int? departmentId;
  String? departmentName;
  String? region;
  String? compCode;
  String? area;
  String? center;
  String? province;
  String? channel;

  Department(
      {this.departmentId,
      this.departmentName,
      this.region,
      this.compCode,
      this.area,
      this.center,
      this.province,
      this.channel});

  Department.fromJson(Map<String, dynamic> json) {
    departmentId = json['departmentId'];
    departmentName = json['departmentName'];
    region = json['region'];
    compCode = json['compCode'];
    area = json['area'];
    center = json['center'];
    province = json['province'];
    channel = json['channel'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['departmentId'] = this.departmentId;
    data['departmentName'] = this.departmentName;
    data['region'] = this.region;
    data['compCode'] = this.compCode;
    data['area'] = this.area;
    data['center'] = this.center;
    data['province'] = this.province;
    data['channel'] = this.channel;
    return data;
  }
}

class Company {
  int? id;
  String? compCode;
  String? companyName;
  int? region;
  String? createdAt;

  Company(
      {this.id, this.compCode, this.companyName, this.region, this.createdAt});

  Company.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    compCode = json['compCode'];
    companyName = json['companyName'];
    region = json['region'];
    createdAt = json['created_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['compCode'] = this.compCode;
    data['companyName'] = this.companyName;
    data['region'] = this.region;
    data['created_at'] = this.createdAt;
    return data;
  }
}
