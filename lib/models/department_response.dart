class DepartmentResponse {
  String? status;
  String? message;
  List<Departments>? departments;

  DepartmentResponse({this.status, this.message, this.departments});

  DepartmentResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    if (json['departments'] != null) {
      departments = [];
      json['departments'].forEach((v) {
        departments!.add(new Departments.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.departments != null) {
      data['departments'] = this.departments!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Departments {
  int? departmentId;
  String? departmentName;
  String? region;
  String? compCode;
  String? area;
  String? center;
  String? province;
  String? channel;

  Departments(
      {this.departmentId,
      this.departmentName,
      this.region,
      this.compCode,
      this.area,
      this.center,
      this.province,
      this.channel});

  Departments.fromJson(Map<String, dynamic> json) {
    departmentId = json['departmentId'];
    departmentName = json['departmentName'];
    region = json['region'];
    compCode = json['compCode'];
    area = json['area'];
    center = json['center'];
    province = json['province'];
    channel = json['channel'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['departmentId'] = this.departmentId;
    data['departmentName'] = this.departmentName;
    data['region'] = this.region;
    data['compCode'] = this.compCode;
    data['area'] = this.area;
    data['center'] = this.center;
    data['province'] = this.province;
    data['channel'] = this.channel;
    return data;
  }
}
