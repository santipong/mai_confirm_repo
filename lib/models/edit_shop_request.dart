import 'package:mai_confirm_asset/models/request_create_template.dart';

class EditShopRequest {
  List<RawData>? rawData;

  EditShopRequest({this.rawData});

  EditShopRequest.fromJson(Map<String, dynamic> json) {
    if (json['rawData'] != null) {
      rawData = [];
      json['rawData'].forEach((v) {
        rawData!.add(new RawData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.rawData != null) {
      data['rawData'] = this.rawData!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
