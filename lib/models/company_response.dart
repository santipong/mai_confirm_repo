class CompaniesResponse {
  String? status;
  String? message;
  List<Companies>? companies;

  CompaniesResponse({this.status, this.message, this.companies});

  CompaniesResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    if (json['companies'] != null) {
      companies = [];
      json['companies'].forEach((v) {
        companies!.add(new Companies.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.companies != null) {
      data['companies'] = this.companies!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Companies {
  int? id;
  String? compCode;
  String? companyName;
  int? region;
  String? createdAt;
  String? address;

  Companies(
      {this.id,
      this.compCode,
      this.companyName,
      this.region,
      this.createdAt,
      this.address});

  Companies.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    compCode = json['compCode'];
    companyName = json['companyName'];
    region = json['region'];
    createdAt = json['created_at'];
    address = json['address'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['compCode'] = this.compCode;
    data['companyName'] = this.companyName;
    data['region'] = this.region;
    data['created_at'] = this.createdAt;
    data['address'] = this.address;
    return data;
  }
}
