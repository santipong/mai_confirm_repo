class CreateTemplateRequest {
  String? periodName;
  String? region;
  String? startDate;
  String? endDate;
  List<RawData>? rawData;

  CreateTemplateRequest(
      {this.periodName,
      this.region,
      this.startDate,
      this.endDate,
      this.rawData});

  CreateTemplateRequest.fromJson(Map<String, dynamic> json) {
    periodName = json['periodName'];
    region = json['region'];
    startDate = json['startDate'];
    endDate = json['endDate'];
    if (json['rawData'] != null) {
      rawData = [];
      json['rawData'].forEach((v) {
        rawData!.add(new RawData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['periodName'] = this.periodName;
    data['region'] = this.region;
    data['startDate'] = this.startDate;
    data['endDate'] = this.endDate;
    if (this.rawData != null) {
      data['rawData'] = this.rawData!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class RawData {
  String? shopId;
  List<RawTemplates>? rawTemplates;

  RawData({this.shopId, this.rawTemplates});

  RawData.fromJson(Map<String, dynamic> json) {
    shopId = json['shopId'];
    if (json['rawTemplates'] != null) {
      rawTemplates = [];
      json['rawTemplates'].forEach((v) {
        rawTemplates!.add(new RawTemplates.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['shopId'] = this.shopId;
    if (this.rawTemplates != null) {
      data['rawTemplates'] = this.rawTemplates!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class RawTemplates {
  String? region;
  String? companyName;
  String? area;
  String? center;
  String? province;
  String? department;
  String? channel;
  String? planDate;
  String? customerCode;
  String? customerName;
  String? address;
  String? location;
  String? employeeId;
  String? employeeName;
  String? accountDate;
  String? assetType;
  String? assetCode;
  String? assetName;
  String? assetStatus;
  String? unitCount;
  String? unitPrice;
  String? accountAmount;

  RawTemplates(
      {this.region,
      this.companyName,
      this.area,
      this.center,
      this.province,
      this.department,
      this.channel,
      this.planDate,
      this.customerCode,
      this.customerName,
      this.address,
      this.location,
      this.employeeId,
      this.employeeName,
      this.accountDate,
      this.assetType,
      this.assetCode,
      this.assetName,
      this.assetStatus,
      this.unitCount,
      this.unitPrice,
      this.accountAmount});

  RawTemplates.fromJson(Map<String, dynamic> json) {
    region = json['region'];
    companyName = json['companyName'];
    area = json['area'];
    center = json['center'];
    province = json['province'];
    department = json['department'];
    channel = json['channel'];
    planDate = json['planDate'];
    customerCode = json['customerCode'];
    customerName = json['customerName'];
    address = json['address'];
    location = json['location'];
    employeeId = json['employeeId'];
    employeeName = json['employeeName'];
    accountDate = json['accountDate'];
    assetType = json['assetType'];
    assetCode = json['assetCode'];
    assetName = json['assetName'];
    assetStatus = json['assetStatus'];
    unitCount = json['unitCount'];
    unitPrice = json['unitPrice'];
    accountAmount = json['accountAmount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['region'] = this.region;
    data['companyName'] = this.companyName;
    data['area'] = this.area;
    data['center'] = this.center;
    data['province'] = this.province;
    data['department'] = this.department;
    data['channel'] = this.channel;
    data['planDate'] = this.planDate;
    data['customerCode'] = this.customerCode;
    data['customerName'] = this.customerName;
    data['address'] = this.address;
    data['location'] = this.location;
    data['employeeId'] = this.employeeId;
    data['employeeName'] = this.employeeName;
    data['accountDate'] = this.accountDate;
    data['assetType'] = this.assetType;
    data['assetCode'] = this.assetCode;
    data['assetName'] = this.assetName;
    data['assetStatus'] = this.assetStatus;
    data['unitCount'] = this.unitCount;
    data['unitPrice'] = this.unitPrice;
    data['accountAmount'] = this.accountAmount;
    return data;
  }
}
