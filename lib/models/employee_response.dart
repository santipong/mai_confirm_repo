class EmployeeResponse {
  String? status;
  String? message;
  List<Employees>? employees;

  EmployeeResponse({this.status, this.message, this.employees});

  EmployeeResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    if (json['employees'] != null) {
      employees = [];
      json['employees'].forEach((v) {
        employees!.add(new Employees.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.employees != null) {
      data['employees'] = this.employees!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Employees {
  int? empId;
  String? empName;
  String? email;
  String? empPosition;
  String? signature;
  String? region;
  String? compCode;
  int? departmentId;

  Employees(
      {this.empId,
      this.empName,
      this.email,
      this.empPosition,
      this.signature,
      this.region,
      this.compCode,
      this.departmentId});

  Employees.fromJson(Map<String, dynamic> json) {
    empId = json['empId'];
    empName = json['empName'];
    email = json['email'];
    empPosition = json['empPosition'];
    signature = json['signature'];
    region = json['region'];
    compCode = json['compCode'];
    departmentId = json['departmentId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['empId'] = this.empId;
    data['empName'] = this.empName;
    data['email'] = this.email;
    data['empPosition'] = this.empPosition;
    data['signature'] = this.signature;
    data['region'] = this.region;
    data['compCode'] = this.compCode;
    data['departmentId'] = this.departmentId;
    return data;
  }
}
