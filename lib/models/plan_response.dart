class PlanResponse {
  String? status;
  String? message;
  List<Periods>? periods;

  PlanResponse({this.status, this.message, this.periods});

  PlanResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    if (json['periods'] != null) {
      periods = [];
      json['periods'].forEach((v) {
        periods!.add(new Periods.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.periods != null) {
      data['periods'] = this.periods!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Periods {
  int? periodId;
  String? periodNName;
  String? regionId;
  String? startDate;
  String? endDate;

  Periods(
      {this.periodId,
      this.periodNName,
      this.regionId,
      this.startDate,
      this.endDate});

  Periods.fromJson(Map<String, dynamic> json) {
    periodId = json['periodId'];
    periodNName = json['periodNName'];
    regionId = json['regionId'];
    startDate = json['startDate'];
    endDate = json['endDate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['periodId'] = this.periodId;
    data['periodNName'] = this.periodNName;
    data['regionId'] = this.regionId;
    data['startDate'] = this.startDate;
    data['endDate'] = this.endDate;
    return data;
  }
}
