class TemplateModel {
  final String? region;
  final String? companyName;
  final String? area;
  final String? center;
  final String? province;
  final String? department;
  final String? channel;
  final String? planDate;
  final String? customerCode;
  final String? customerName;
  final String? address;
  final String? location;
  final String? employeeId;
  final String? employeeName;
  final String? accountDate;
  final String? assetType;
  final String? assetCode;
  final String? assetName;
  final String? assetStatus;
  final String? unitCount;
  final String? unitPrice;
  final String? accountAmount;

  TemplateModel(
      {this.region,
      this.companyName,
      this.area,
      this.center,
      this.province,
      this.department,
      this.channel,
      this.planDate,
      this.customerCode,
      this.customerName,
      this.address,
      this.location,
      this.employeeId,
      this.employeeName,
      this.accountDate,
      this.assetType,
      this.assetCode,
      this.assetName,
      this.assetStatus,
      this.unitCount,
      this.unitPrice,
      this.accountAmount});
}
