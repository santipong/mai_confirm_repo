class LoginResponse {
  String? status;
  String? message;
  String? avaliableRegion;
  User? user;

  LoginResponse({this.status, this.message, this.avaliableRegion, this.user});

  LoginResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    avaliableRegion = json['avaliableRegion'];
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    data['avaliableRegion'] = this.avaliableRegion;
    if (this.user != null) {
      data['user'] = this.user!.toJson();
    }
    return data;
  }
}

class User {
  int? empId;
  String? empName;
  String? email;
  String? empPosition;
  String? signature;
  String? region;
  String? compCode;

  User(
      {this.empId,
      this.empName,
      this.email,
      this.empPosition,
      this.signature,
      this.region,
      this.compCode});

  User.fromJson(Map<String, dynamic> json) {
    empId = json['empId'];
    empName = json['empName'];
    email = json['email'];
    empPosition = json['empPosition'];
    signature = json['signature'];
    region = json['region'];
    compCode = json['compCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['empId'] = this.empId;
    data['empName'] = this.empName;
    data['email'] = this.email;
    data['empPosition'] = this.empPosition;
    data['signature'] = this.signature;
    data['region'] = this.region;
    data['compCode'] = this.compCode;
    return data;
  }
}
