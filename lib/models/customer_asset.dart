import 'package:mai_confirm_asset/models/template.dart';

class CustomerAsset {
  final String? customerCode;
  final List<TemplateModel>? assets;

  CustomerAsset({
    this.customerCode,
    this.assets,
  });
}
