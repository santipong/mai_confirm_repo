class ApiClient {
  static const STATUS_SUCCESS = "success";
  static const STATUS_ERROR = "error";

  static const BASE_URL = "https://ss.thaibev.com/mai-confirm-prd/api/"; //PRD
  //static const BASE_URL = "https://ss.thaibev.com/mai-confirm/api/"; //UAT
  //static const BASE_URL = "https://ss.thaibev.com/mai-confirm-asset/api/"; //DEV
  //static const BASE_URL = "https://localhost:44340/api/";

  static const CREATE_PERIOD = BASE_URL + "createPeriod";
  static const EDIT_SHOP = BASE_URL + "editShop";
  static const EDIT_PERIOD = BASE_URL + "editPeriod";
  static const LOGIN = BASE_URL + "login";
  static const GET_PLAN = BASE_URL + "getPlan";
  static const GET_COMPANY = BASE_URL + "getCompanies";
  static const GET_TASK = BASE_URL + "getTask";
  static const GET_EMPLOYEE_BY_PERIOD = BASE_URL + "getEmployeeByPeriod";
  static const DELETE_TASK = BASE_URL + "deleteTask";
  static const DOWNLOAD_KEEPER_REPORT = BASE_URL + "downloadReportForKeeper/";
  static const DOWNLOAD_DIFF_REPORT = BASE_URL + "downloadDiffReport/";
  static const DOWNLOAD_PDF_REPORT =
      BASE_URL + "ExportFile/" + "GenerateConsentFormToPdf?";
  static const GET_DEPARTMENT_BY_PERIOD = BASE_URL + "getDepartmentByPeriod";
  static const GET_DEPARTMENT_BY_PERIOD_CENTER =
      BASE_URL + "getDepartmentByPeriodDistinctCenter";
  static const DOWNLOAD_REPORT__BY_COMP_CODE_AND_CENTER =
      BASE_URL + "downloadReportByCenter/";
  static const DOWNLOAD_REPORT__BY_DEPARTMENT =
      BASE_URL + "downloadReportByDepartment/";
  static const DOWNLOAD_REPORT__BY_EMPLOYEE =
      BASE_URL + "downloadReportByEmployee/";

  static const DOWNLOAD_REPORT_WITH_IMAGE__BY_COMP_CODE_AND_CENTER =
      BASE_URL + "ExportFile/GenerateConsentFormToExcel/";
  static const DOWNLOAD_REPORT_WITH_IMAGE__BY_DEPARTMENT =
      BASE_URL + "downloadReportWithImageDepartment/";
  static const DOWNLOAD_REPORT_WITH_IMAGE__BY_EMPLOYEE =
      BASE_URL + "downloadReportWithImageEmployee/";
}
