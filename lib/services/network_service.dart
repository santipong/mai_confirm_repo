import 'package:dio/dio.dart';
import 'package:mai_confirm_asset/models/company_response.dart';
import 'package:mai_confirm_asset/models/department_response.dart';
import 'package:mai_confirm_asset/models/edit_shop_request.dart';
import 'package:mai_confirm_asset/models/employee_response.dart';
import 'package:mai_confirm_asset/models/login_request.dart';
import 'package:mai_confirm_asset/models/login_response.dart';
import 'package:mai_confirm_asset/models/plan_response.dart';
import 'package:mai_confirm_asset/models/request_create_template.dart';
import 'package:mai_confirm_asset/models/response.dart';
import 'package:mai_confirm_asset/models/task_response.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

import 'api_client.dart';

class NetworkService {
  static final _instance = NetworkService._internal();

  static const int STATUS_200 = 200;
  static const int STATUS_201 = 201;
  static const int STATUS_404 = 404;

  static const String SUCCESS = "success";
  static const String ERROR = "error";
  static const String CONTENT_TYPE_HEADER = "application/json; charset=utf-8";

  NetworkService._internal();

  factory NetworkService() => _instance;

  static final _dio = createDio();

  static Dio createDio() {
    var dio = Dio();
    dio.interceptors.add(PrettyDioLogger(
        requestBody: true, responseBody: true, error: true, maxWidth: 90));
    dio.options.headers['content-Type'] = 'application/json';
    dio.options.headers['Access-Control-Allow-Origin'] = '*';
    return dio;
  }

  Options getOptions() {
    return Options(headers: {
      Headers.contentTypeHeader: CONTENT_TYPE_HEADER,
    });
  }

  Future<ApiResponse> createPeriod(CreateTemplateRequest request) async {
    Response response = await _dio.post(
      ApiClient.CREATE_PERIOD,
      data: request.toJson(),
      options: getOptions(),
    );
    if (response.statusCode == STATUS_200) {
      return ApiResponse.fromJson(response.data);
    }
    throw Exception(response.statusMessage);
  }

  Future<ApiResponse> editShop(EditShopRequest request, String taskId) async {
    Response response = await _dio.put(
      ApiClient.EDIT_SHOP + "/" + taskId.toString(),
      data: request.toJson(),
      options: getOptions(),
    );
    if (response.statusCode == STATUS_200) {
      return ApiResponse.fromJson(response.data);
    }
    throw Exception(response.statusMessage);
  }

  Future<ApiResponse> editPeriod(
      CreateTemplateRequest request, String periodId) async {
    Response response = await _dio.put(
      ApiClient.EDIT_PERIOD + "/" + periodId,
      data: request.toJson(),
      options: getOptions(),
    );
    if (response.statusCode == STATUS_200) {
      return ApiResponse.fromJson(response.data);
    }
    throw Exception(response.statusMessage);
  }

  Future<LoginResponse> login(LoginRequest request) async {
    Response response = await _dio.post(
      ApiClient.LOGIN,
      data: request.toJson(),
      options: getOptions(),
    );
    if (response.statusCode == STATUS_200) {
      return LoginResponse.fromJson(response.data);
    }
    throw Exception(response.statusMessage);
  }

  Future<PlanResponse> getPlan(String region) async {
    Response response = await _dio.get(
      ApiClient.GET_PLAN + "/" + region,
      options: getOptions(),
    );
    if (response.statusCode == STATUS_200) {
      return PlanResponse.fromJson(response.data);
    }
    throw Exception(response.statusMessage);
  }

  Future<TaskResponse> getTask(String periodId) async {
    Response response = await _dio.get(
      ApiClient.GET_TASK + "/" + periodId,
      options: getOptions(),
    );
    if (response.statusCode == STATUS_200) {
      return TaskResponse.fromJson(response.data);
    }
    throw Exception(response.statusMessage);
  }

  Future<EmployeeResponse> getEmployeeByPeriod(String periodId) async {
    Response response = await _dio.get(
      ApiClient.GET_EMPLOYEE_BY_PERIOD + "/" + periodId,
      options: getOptions(),
    );
    if (response.statusCode == STATUS_200) {
      return EmployeeResponse.fromJson(response.data);
    }
    throw Exception(response.statusMessage);
  }

  Future<ApiResponse> deleteTask(String taskId) async {
    Response response = await _dio.delete(
      ApiClient.DELETE_TASK + "/" + taskId,
      options: getOptions(),
    );
    if (response.statusCode == STATUS_200) {
      return ApiResponse.fromJson(response.data);
    }
    throw Exception(response.statusMessage);
  }

  Future<DepartmentResponse> getDepartmentByPeriod(String periodId) async {
    Response response = await _dio.get(
      ApiClient.GET_DEPARTMENT_BY_PERIOD + "/" + periodId,
      options: getOptions(),
    );
    if (response.statusCode == STATUS_200) {
      return DepartmentResponse.fromJson(response.data);
    }
    throw Exception(response.statusMessage);
  }

  Future<DepartmentResponse> getDepartmentByPeriodCenter(
      String periodId) async {
    Response response = await _dio.get(
      ApiClient.GET_DEPARTMENT_BY_PERIOD_CENTER + "/" + periodId,
      options: getOptions(),
    );
    if (response.statusCode == STATUS_200) {
      return DepartmentResponse.fromJson(response.data);
    }
    throw Exception(response.statusMessage);
  }

  Future<CompaniesResponse> getCompany(String region) async {
    Response response = await _dio.get(
      ApiClient.GET_COMPANY + "/" + region,
      options: getOptions(),
    );
    if (response.statusCode == STATUS_200) {
      return CompaniesResponse.fromJson(response.data);
    }
    throw Exception(response.statusMessage);
  }
}
