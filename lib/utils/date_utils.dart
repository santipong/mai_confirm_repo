import 'package:intl/intl.dart';

String convertDate(String strDate) {
  var dateTime = DateFormat("yyyy-MM-dd").parse(strDate);
  return DateFormat('dd-MM-yyyy').format(dateTime);
}

String convertDateToFormat(DateTime dateTime) {
  DateFormat formatter = DateFormat('dd-MM-yyyy');
  return formatter.format(dateTime);
}

String convertToMonthFormat(DateTime dateTime) {
  DateFormat formatter = DateFormat('MM-yyyy');
  return formatter.format(dateTime);
}

String convertDateToRequestFormat(String strDate) {
  var dateTime = DateFormat('dd-MM-yyyy').parse(strDate);
  return DateFormat("yyyy-MM-dd").format(dateTime);
}

String getCurrentDateTime() {
  return DateFormat("dd-MM-yyyy HH:mm:ss").format(DateTime.now());
}

String convertToRequestMonthFormat(DateTime dateTime) {
  DateFormat formatter = DateFormat("MM-yyyy");
  return formatter.format(dateTime);
}

DateTime convertStringToDateTime(String date) {
  return DateFormat('dd-MM-yyyy').parse(date);
}

String convertToDownloadMonthFormat(DateTime dateTime) {
  DateFormat formatter = DateFormat('MM/yyyy');
  return formatter.format(dateTime);
}

DateTime convertStringRawToDateTime(String date) {
  return DateFormat('yyyy-MM-dd').parse(date);
}

