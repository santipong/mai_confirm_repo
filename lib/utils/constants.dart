import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xFF76CFE3);
const kSecondaryColor = Color(0XFFFFC906);
const kTextColor = Color(0XFF282828);
const kBgLightColor = Color(0xFFF2F4FC);
const kBgDarkColor = Color(0xFFEBEDFA);
const kBadgeColor = Color(0xFFEE376E);
const kGrayColor = Color(0xFF8793B2);
const kTitleTextColor = Color(0xFF30384D);
const kDefaultPadding = 20.0;
const kRed = Color(0XFFFB4D4D);
const kWhite = Color(0xFFffffff);
const kGrayBg = Color(0xFFf2f2f2);

const double kPadding = 20;
const double kAvatarRadius = 45;

const PAGE_CREATE_PLAN = 'create_plan';
const PAGE_VIEW_PLAN = 'view_plan';
const PAGE_REPORT = 'report';

enum Option { LogIn, SignUp }
