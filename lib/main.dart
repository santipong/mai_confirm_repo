import 'package:flutter/material.dart';
import 'package:flutter_web_plugins/flutter_web_plugins.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:jwt_decode/jwt_decode.dart';
import 'package:mai_confirm_asset/check_token_page.dart';
import 'package:mai_confirm_asset/page/home/home_loading_page.dart';
import 'package:mai_confirm_asset/page/plan/plan_page.dart';
import 'package:mai_confirm_asset/page/report/report_page.dart';
import 'package:mai_confirm_asset/utils/constants.dart';
import 'package:mai_confirm_asset/page/home/home_page.dart';

void main() {
  setUrlStrategy(PathUrlStrategy());
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'MAI ยืนยันยอดลูกหนี้',
      theme: ThemeData(
          primaryColor: kPrimaryColor,
          visualDensity: VisualDensity.adaptivePlatformDensity,
          textTheme: GoogleFonts.poppinsTextTheme(Theme.of(context).textTheme)),
      debugShowCheckedModeBanner: false,
      initialRoute: Routes.login,
      onGenerateRoute: generateRoute,
    );
  }
}

class HexColor extends Color {
  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));

  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll('#', '');
    if (hexColor.length == 6) {
      hexColor = 'FF' + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }
}

Route<dynamic> generateRoute(RouteSettings settings) {
  String token = _getToken();
  switch (settings.name!.replaceAll("?token=" + token, "")) {
    case Routes.defaultPage:
      return MaterialPageRoute(
        builder: (_) => HomePage(
          token: token,
        ),
        settings: settings,
      );
    case Routes.portal:
      return MaterialPageRoute(
        builder: (_) => HomeLoadingPage(
          token: token,
        ),
        settings: settings,
      );
    case Routes.login:
      return MaterialPageRoute(
        builder: (_) => HomePage(
          token: token,
        ),
        settings: settings,
      );
    case Routes.createPlan:
      return MaterialPageRoute(
        builder: (_) => PlanPage(),
        settings: settings,
      );
    case Routes.report:
      return MaterialPageRoute(
        builder: (_) => ReportPage(),
        settings: settings,
      );
    default:
      return MaterialPageRoute(
        builder: (_) => Scaffold(
            body: Center(child: Text('Unknown route ${settings.name}'))),
        settings: settings,
      );
  }
}

String _getToken() {
  try {
    String? token = Uri.base.queryParameters["token"];
    if (token != null) {
      return token;
    } else {
      return "";
    }
  } catch (e) {
    return "";
  }
}

class Routes {
  static const String defaultPage = '/';
  static const String login = '/login';
  static const String createPlan = '/createPlan';
  static const String report = '/report';
  static const String portal = '/portal';
}
