import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:mai_confirm_asset/main.dart';
import 'package:mai_confirm_asset/models/login_response.dart';
import 'package:mai_confirm_asset/page/login/login_page.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CheckTokenPage extends StatefulWidget {
  final String? token;
  const CheckTokenPage({
    Key? key,
    this.token,
  }) : super(key: key);

  @override
  _CheckTokenPageState createState() => _CheckTokenPageState();
}

class _CheckTokenPageState extends State<CheckTokenPage> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<LoginResponse>(
      future: getLoginData(),
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          Navigator.pushNamed(context, Routes.login);
        }
        if (snapshot.hasData) {
         
        }
        return buildLoading();
      },
    );
  }

  Widget buildLoading() {
    return Center(
      child: Padding(
          padding: EdgeInsets.all(10.0), child: CircularProgressIndicator()),
    );
  }

  Future<LoginResponse> getLoginData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String rawJson = prefs.getString(LoginPage.loginKey)!;
    Map<String, dynamic> json = jsonDecode(rawJson);
    return LoginResponse.fromJson(json);
  }
}
